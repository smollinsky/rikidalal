"use strict";
importScripts("sw-toolbox.js");
toolbox.precache(["index.html","home.html","assets/css/main.min.css","assets/fonts/*"]);
toolbox.router.get("/assets/img/content*", toolbox.cacheFirst); toolbox.router.get("/*", toolbox.networkFirst, { networkTimeoutSeconds: 5});