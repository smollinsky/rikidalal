<?php
class ModelLocalisationLocation extends Model {
	public function getLocation($location_id) {
		$query = $this->db->query("SELECT location_id, country, city, address, telephone, email, image, address1, comment, continent_id, has_noya, has_riki, sort_order FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

		return $query->row;
	}

    public function getLocations($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "location";

        $query = $this->db->query($sql);

        return $query->rows;
    }

}