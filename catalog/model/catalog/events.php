<?php

class ModelCatalogEvents extends Model {

    public function getEvents($data = array()) {
        $sql = "SELECT e.*, ed.* FROM " . DB_PREFIX . "events e LEFT JOIN " . DB_PREFIX . "event_description ed ON (e.event_id = ed.event_id) WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }


}
?>