<?php
class ControllerExtensionModuleBanner2 extends Controller {
    public function index($setting) {
        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $data['text_collections'] = $this->language->get('text_collections');

        $data['banners'] = array();

        $results = $this->model_design_banner->getBanner($setting['banner_id']);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
                );
            }
        }

        $data['module'] = $module++;

        return $this->load->view('extension/module/banner2', $data);
    }
}