<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
        $this->load->language('extension/module/carousel');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_text'] = $this->language->get('heading_text');

		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
				);
			}
		}

		$data['module'] = $module++;

		return $this->load->view('extension/module/carousel', $data);
	}
}