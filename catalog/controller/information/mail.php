<?php
class ControllerInformationMail extends Controller {

public function index() {


$this->response->setOutput($this->load->view('information/mail'));

}

public function send() {


	header('Content-Type: application/json');
header('Access-Control-Allow-Origin: https://' . $_SERVER['HTTP_HOST']);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Expose-Headers: AMP-Access-Control-Allow-Source-Origin');
header('AMP-Access-Control-Allow-Source-Origin: https://' . $_SERVER['HTTP_HOST']);

// Мы можем обрабатывать данные $ _POST
$data = array();


if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$name = strip_tags(trim($_POST["contact-name"]));
	$name = str_replace(array("\r", "\n"), array(" ", " "), $name);
	$email = trim($_POST["contact-email"]);
	$message = trim($_POST["contact-message"]);

	if (empty($name) OR empty($email)) {
		http_response_code(400);
		exit;
	}


	$recipient = "avocode20@gmail.com"; // замените на ваш email адрес
	$subject = "Email с сайта $thisDomain, AMP обучение, от посетителя: $name";

	$email_content = "От кого: $name\n\n";
	$email_content .= "Email: $email\n\n";
	$email_content .= "Сообщение:\n\n $message";

	$email_headers = "From: $name <$email>";

	if (mail($recipient, $subject, $email_content, $email_headers)) {
		$data = array();
		$data['name'] = $_POST['contact-name'];
		echo json_encode($data);

	} else {
		http_response_code(500);
		exit;
	}

} else {
	http_response_code(403);
	exit;
}

// Наконец, результат возвращается в формате JSON
echo json_encode($data);


}


}
?>