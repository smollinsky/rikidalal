<?php
class ControllerCommonRequest extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_request'),
            'href' => $this->url->link('common/request')
        );

        $data['info'] = 0;


        $data['actions'] = $this->url->link('common/request', '', true);

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['firstname'])) {
            $data['firstname'] = $this->request->post['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['phone'])) {
            $data['phone'] = $this->request->post['phone'];
        } else {
            $data['phone'] = '';
        }

        if (isset($this->request->post['date'])) {
            $data['date'] = $this->request->post['date'];
        } else {
            $data['date'] = '';
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $hubspotutk      = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
            $ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
            $hs_context      = array(
                'hutk' => $hubspotutk,
                'ipAddress' => $ip_addr,
                'pageUrl' => '',
                'pageName' => 'Request an appointment'
            );
            $hs_context_json = json_encode($hs_context);

            //Need to populate these variable with values from the form.
            $str_post = "firstname=" . urlencode($this->request->post['name']) 
                . "&lastname=" . urlencode($this->request->post['firstname']) 
                . "&email=" . urlencode($this->request->post['email']) 
                . "&phone=" . urlencode($this->request->post['phone']) 
                . "&date=" . urlencode($this->request->post['date']) 
                . "&hs_context=" . urlencode($hs_context_json); //Leave this one be

            //replace the values in this URL with your portal ID and your form GUID
            $endpoint = 'https://forms.hubspot.com/uploads/form/v2/2761440/784cee83-a6fe-4dfb-8500-bd649488dd26';

            $ch = @curl_init();
            @curl_setopt($ch, CURLOPT_POST, true);
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
            @curl_setopt($ch, CURLOPT_URL, $endpoint);
            @curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded'
            ));
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response    = @curl_exec($ch); //Log the response from HubSpot as needed.
            $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
            @curl_close($ch);
            echo $status_code . " " . $response;

            $this->response->redirect($this->url->link('common/request'));
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/request', $data));
    }

    public function autocomplete(){
        if(!empty($_POST["search"])){

            $search = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["search"]))));

            $db_search = $this->db->query("SELECT * FROM oc_location WHERE city LIKE '%$search%'");

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();           

            foreach($db_search->rows as $result){
                 foreach ($data['countries'] as $country) {
                if ($country['country_id'] == $result['country']) {         
                echo "\n<li><span class='value'>".$result["city"]."</span> <span>/ ".$country["name"]."</span></li>";
                   }
            }
            }
        }
    }

    public function count(){
    if (isset($this->request->get['city'])) {
            $city = $this->request->get['city'];   
            $data['info_count'] = array();     
        $data['info_count'] = $this->db->query("SELECT COUNT(city) FROM oc_location WHERE city = '" . $city . "'");
        $data['info'] = $data['info_count']->rows[0]['COUNT(city)'];
        $this->response->setOutput($this->load->view('common/count', $data));
    }
    }

    public function show_autocomplete_result(){
        if (isset($this->request->get['city'])) {
            $city = $this->request->get['city'];

            $data['info_search'] = array();

            $data['info_search'] = $this->db->query("SELECT * FROM oc_location WHERE city = '" . $city . "'");
            
            $this->load->language('common/stores');

            $data['text_detail'] = $this->language->get('text_detail');

            $data['locations'] = array();

            $this->load->model('localisation/location');

            $data['locations'] = $this->model_localisation_location->getLocations();

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();

            foreach($data['locations'] as $location_id) {
                $location_info = $this->model_localisation_location->getLocation($location_id);

                if ($location_info) {
                    if ($location_info['image']) {
                        $image = $this->model_tool_image->resize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
                    } else {
                        $image = false;
                    }

                    $data['locations'][] = array(
                        'location_id' => $location_info['location_id'],
                        'country'        => $location_info['country'],
                        'city'        => $location_info['city'],
                        'address'     => nl2br($location_info['address']),
                        'continent_id'     => $location_info['continent_id'],
                        'telephone'   => $location_info['telephone'],
                        'email'         => $location_info['email'],
                        'image'       => $image,
                        'address1'        => nl2br($location_info['address1']),
                        'comment'     => $location_info['comment'],
                        'status'     => $location_info['status'],
                        'sort_order'     => $location_info['sort_order'],
                        'has_riki'     => $location_info['has_riki'],
                        'has_noya'     => $location_info['has_noya']
                    );
                }
            }

            foreach ($data['info_search']->rows as $result) {
                $data['info_search1'] = array(
                    'location_id' => $result['location_id'],
                    'country'        => $result['country'],
                    'city'        => $result['city'],
                    'address'     => nl2br($result['address']),
                    'continent_id'     => $result['continent_id'],
                    'telephone'   => $result['telephone'],
                    'email'         => $result['email'],
                    'address1'        => nl2br($result['address1']),
                    'comment'     => $result['comment'],
                    'status'     => $result['status'],
                    'sort_order'     => $result['sort_order'],
                    'has_riki'     => $result['has_riki'],
                    'has_noya'     => $result['has_noya']
                );
            }

            $this->response->setOutput($this->load->view('common/request_stores', $data));
        }

    }
}
