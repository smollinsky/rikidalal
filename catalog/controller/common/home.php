<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

        $this->load->language('common/home');

        $data['text_search1'] = $this->language->get('text_search1');
        $data['text_search2'] = $this->language->get('text_search2');
        $data['text_search3'] = $this->language->get('text_search3');
        $data['text_search4'] = $this->language->get('text_search4');
        $data['text_search5'] = $this->language->get('text_search5');
        $data['text_contact1'] = $this->language->get('text_contact1');
        $data['text_contact2'] = $this->language->get('text_contact2');
        $data['text_contact3'] = $this->language->get('text_contact3');
        $data['text_contact4'] = $this->language->get('text_contact4');
        $data['text_contact5'] = $this->language->get('text_contact5');
        $data['text_contact6'] = $this->language->get('text_contact6');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}

    public function send_form() {

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 100)) {
                $json['error1'] = 'Name required';
            }

            if ((utf8_strlen($this->request->post['email']) < 6) || (utf8_strlen($this->request->post['email']) > 100)) {
                $json['error2'] = 'Email required';
            }

            if ((utf8_strlen($this->request->post['message']) < 15) || (utf8_strlen($this->request->post['message']) > 2500)) {
                $json['error3'] = 'Message required';
            }

            $text = '';
            if (!isset($json['error1']) && !isset($json['error2']) && !isset($json['error3']) ) {
                if(!$this->request->post['message']) $this->request->post['name'] = 'Anonim';
                $html = '<h3>Mail from '.$this->request->post['name'].'</h3>';
                if($this->request->post['name'])$html .= '<p><b>Name:</b> '.$this->request->post['name'].'</p>';
                $html .= '<p><b>Email:</b> '.$this->request->post['email'].'</p>';
                if($this->request->post['message']) $html .= '<p><b>Message:</b> '.$this->request->post['message'].'</p>';

                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->hostname = $this->config->get('config_smtp_host');
                $mail->username = $this->config->get('config_smtp_username');
                $mail->password = $this->config->get('config_smtp_password');
                $mail->port = $this->config->get('config_smtp_port');
                $mail->timeout = $this->config->get('config_smtp_timeout');
                $mail->setTo($this->config->get('config_email'));
                $mail->setFrom($this->config->get('config_email'));
                $mail->setSender($this->request->post['name']);
                $mail->setSubject(html_entity_decode('Theme mail '.$this->request->post['name'], ENT_QUOTES, 'UTF-8'));
                $mail->setHtml($html);
                $mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
                $mail->send();
                $json['success'] = true;
            }
        }

        $this->response->setOutput(json_encode($json));
    }
}
