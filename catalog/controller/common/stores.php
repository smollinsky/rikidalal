<?php
class ControllerCommonStores extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->load->language('common/stores');

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('common/stores')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_location'] = $this->language->get('text_location');
        $data['text_store'] = $this->language->get('text_store');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_address'] = $this->language->get('text_address');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_open'] = $this->language->get('text_open');
        $data['text_comment'] = $this->language->get('text_comment');

        $data['text_botique_search1'] = $this->language->get('text_botique_search1');
        $data['text_botique_search2'] = $this->language->get('text_botique_search2');
        $data['text_botique_search3'] = $this->language->get('text_botique_search3');
        $data['text_botique_search4'] = $this->language->get('text_botique_search4');

        $data['text_botique'] = $this->language->get('text_botique');
        $data['text_botique1'] = $this->language->get('text_botique1');
        $data['text_botique2'] = $this->language->get('text_botique2');
        $data['text_botique3'] = $this->language->get('text_botique3');
        $data['text_detail'] = $this->language->get('text_detail');

        $data['locations'] = array();

        $this->load->model('localisation/location');

        $data['locations'] = $this->model_localisation_location->getLocations();

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        foreach($data['locations'] as $location_id) {
            $location_info = $this->model_localisation_location->getLocation($location_id);

            if ($location_info) {
                if ($location_info['image']) {
                    $image = $this->model_tool_image->resize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
                } else {
                    $image = false;
                }

                $data['locations'][] = array(
                    'location_id' => $location_info['location_id'],
                    'country'        => $location_info['country'],
                    'city'        => $location_info['city'],
                    'address'     => nl2br($location_info['address']),
                    'continent_id'     => $location_info['continent_id'],
                    'telephone'   => $location_info['telephone'],
                    'email'         => $location_info['email'],
                    'image'       => $image,
                    'address1'        => nl2br($location_info['address1']),
                    'comment'     => $location_info['comment'],
                    'status'     => $location_info['status'],
                    'sort_order'     => $location_info['sort_order'],
                    'has_riki'     => $location_info['has_riki'],
                    'has_noya'     => $location_info['has_noya']
                );
            }
        }

        $data['unique_location'] = array();

        foreach ($data['locations'] as $key) {
            if (!count($data['unique_location'])) {
                $data['unique_location'][] = $key;
            } else {
                $unique = 1;
                foreach ($data['unique_location'] as $check) {
                    if ( $check['country'] ==  $key['country'] ) {
                        $unique = 0;
                    }
                }
                if ( $unique ) {
                    $data['unique_location'][] = $key;
                }
            }
        }

        usort($data['unique_location'], function($a,$b){
            return ($a['country']-$b['country']);
        });


        $data['heading_title'] = $this->language->get('heading_title');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/stores', $data));
    }

    public function getBoutiques(){
        if (isset($this->request->get['id'])) {
            $id= (int)$this->request->get['id'];

            $data['info_search'] = array();

            $data['info_search'] = $this->db->query("SELECT * FROM oc_location WHERE country = '" . $id . "'");

            $this->load->language('common/stores');

            $data['text_detail'] = $this->language->get('text_detail');

            $data['locations'] = array();

            $this->load->model('localisation/location');

            $data['locations'] = $this->model_localisation_location->getLocations();

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();

            foreach($data['locations'] as $location_id) {
                $location_info = $this->model_localisation_location->getLocation($location_id);

                if ($location_info) {
                    if ($location_info['image']) {
                        $image = $this->model_tool_image->resize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
                    } else {
                        $image = false;
                    }

                    $data['locations'][] = array(
                        'location_id' => $location_info['location_id'],
                        'country'        => $location_info['country'],
                        'city'        => $location_info['city'],
                        'address'     => nl2br($location_info['address']),
                        'continent_id'     => $location_info['continent_id'],
                        'telephone'   => $location_info['telephone'],
                        'email'         => $location_info['email'],
                        'image'       => $image,
                        'address1'        => nl2br($location_info['address1']),
                        'comment'     => $location_info['comment'],
                        'status'     => $location_info['status'],
                        'sort_order'     => $location_info['sort_order'],
                        'has_riki'     => $location_info['has_riki'],
                        'has_noya'     => $location_info['has_noya']
                    );
                }
            }

            foreach ($data['info_search']->rows as $result) {
                $data['info_search1'] = array(
                    'location_id' => $result['location_id'],
                    'country'        => $result['country'],
                    'city'        => $result['city'],
                    'address'     => nl2br($result['address']),
                    'continent_id'     => $result['continent_id'],
                    'telephone'   => $result['telephone'],
                    'email'         => $result['email'],
                    'address1'        => nl2br($result['address1']),
                    'comment'     => $result['comment'],
                    'status'     => $result['status'],
                    'sort_order'     => $result['sort_order'],
                    'has_riki'     => $result['has_riki'],
                    'has_noya'     => $result['has_noya']
                );
            }

        $this->response->setOutput($this->load->view('common/storeses', $data));
        }
    }

    public function autocomplete(){
        if(!empty($_POST["search"])){

            $search = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["search"]))));

            $db_search = $this->db->query("SELECT * FROM oc_location WHERE city LIKE '%$search%'");

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();           

            foreach($db_search->rows as $result){
                 foreach ($data['countries'] as $country) {
                if ($country['country_id'] == $result['country']) {         
                echo "\n<li><span class='value'>".$result["city"]."</span> <span>/ ".$country["name"]."</span></li>";
                   }
            }
            }
        }
    }

    public function show_autocomplete_result(){
        if (isset($this->request->get['city'])) {
            $city = $this->request->get['city'];

            $data['info_search'] = array();

            $data['info_search'] = $this->db->query("SELECT * FROM oc_location WHERE city = '" . $city . "'");

            $this->load->language('common/stores');

            $data['text_detail'] = $this->language->get('text_detail');

            $data['locations'] = array();

            $this->load->model('localisation/location');

            $data['locations'] = $this->model_localisation_location->getLocations();

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();

            foreach($data['locations'] as $location_id) {
                $location_info = $this->model_localisation_location->getLocation($location_id);

                if ($location_info) {
                    if ($location_info['image']) {
                        $image = $this->model_tool_image->resize($location_info['image'], $this->config->get($this->config->get('config_theme') . '_image_location_width'), $this->config->get($this->config->get('config_theme') . '_image_location_height'));
                    } else {
                        $image = false;
                    }

                    $data['locations'][] = array(
                        'location_id' => $location_info['location_id'],
                        'country'        => $location_info['country'],
                        'city'        => $location_info['city'],
                        'address'     => nl2br($location_info['address']),
                        'continent_id'     => $location_info['continent_id'],
                        'telephone'   => $location_info['telephone'],
                        'email'         => $location_info['email'],
                        'image'       => $image,
                        'address1'        => nl2br($location_info['address1']),
                        'comment'     => $location_info['comment'],
                        'status'     => $location_info['status'],
                        'sort_order'     => $location_info['sort_order'],
                        'has_riki'     => $location_info['has_riki'],
                        'has_noya'     => $location_info['has_noya']
                    );
                }
            }

            foreach ($data['info_search']->rows as $result) {
                $data['info_search1'] = array(
                    'location_id' => $result['location_id'],
                    'country'        => $result['country'],
                    'city'        => $result['city'],
                    'address'     => nl2br($result['address']),
                    'continent_id'     => $result['continent_id'],
                    'telephone'   => $result['telephone'],
                    'email'         => $result['email'],
                    'address1'        => nl2br($result['address1']),
                    'comment'     => $result['comment'],
                    'status'     => $result['status'],
                    'sort_order'     => $result['sort_order'],
                    'has_riki'     => $result['has_riki'],
                    'has_noya'     => $result['has_noya']
                );
            }

            $this->response->setOutput($this->load->view('common/storeses', $data));
        }

    }

}
