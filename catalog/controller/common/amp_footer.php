<?php
class ControllerCommonAmpFooter extends Controller {

    public function addToNewsletter(){

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: https://' . $_SERVER['HTTP_HOST']);
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Expose-Headers: AMP-Access-Control-Allow-Source-Origin');
        header('AMP-Access-Control-Allow-Source-Origin: https://' . $_SERVER['HTTP_HOST']);

        $data = array();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $email = $_POST['newsletter_email'];

            $this->load->language('common/footer');
            $this->load->model('account/customer');

            $this->createNewsletterTables();
            $data['msg'] = $this->language->get('text_success_subcribe');
            $count = $this->checkEmailSubscribe($email);

            if($count == 0){
                $newsletter_id = $this->model_account_customer->addToNewsletter($email);

            } else {
                $data['msg'] = $this->language->get('text_error_subcribe');
            }
            echo $data['msg'];
        }

    }

    public function createNewsletterTables() {
        $query = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "newsletter (
                        `id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
                        `email` VARCHAR( 255 ) NOT NULL ,
                        `group` VARCHAR( 25 ) NOT NULL ,
                        `date_added` DATETIME NOT NULL ,
                        PRIMARY KEY ( `id` )
                        )");
    }

    public function checkEmailSubscribe($email){
        $this->load->model('account/customer');
        $count = $this->model_account_customer->checkEmailSubscribe($email);
        return $count;
    }


	public function index() {
		$data = $this->load->language('common/footer');

        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_newsletter_text'] = $this->language->get('text_newsletter_text');
        $data['text_subcribe'] = $this->language->get('text_subcribe');
        $data['text_error_subcribe'] = $this->language->get('text_error_subcribe');
        $data['text_success_subcribe'] = $this->language->get('text_success_subcribe');
        $data['text_menu'] = $this->language->get('text_menu');
        $data['text_followus'] = $this->language->get('text_followus');

		        // Menu
        $this->load->model('design/custommenu');
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        if ($this->config->get('configcustommenu_custommenu')) {
            $custommenus = $this->model_design_custommenu->getcustommenus();
            $custommenu_child = $this->model_design_custommenu->getChildcustommenus();

            foreach($custommenus as $id => $custommenu) {
                $children_data = array();

                foreach($custommenu_child as $child_id => $child_custommenu) {
                    if (($custommenu['custommenu_id'] != $child_custommenu['custommenu_id']) or !is_numeric($child_id)) {
                        continue;
                    }

                    $child_name = '';

                    if (($custommenu['custommenu_type'] == 'category') and ($child_custommenu['custommenu_type'] == 'category')){
                        $filter_data = array(
                            'filter_category_id'  => $child_custommenu['link'],
                            'filter_sub_category' => true
                        );

                        $child_name = ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '');
                    }

                    $children_data[] = array(
                        'name' => $child_custommenu['name'] . $child_name,
                        'href' => $this->getcustommenuLink($custommenu, $child_custommenu)
                    );
                }

                $data['categories'][] = array(
                    'name'     => $custommenu['name'] ,
                    'children' => $children_data,
                    'column'   => $custommenu['columns'] ? $custommenu['columns'] : 1,
                    'href'     => $this->getcustommenuLink($custommenu)
                );
            }

        } else {

            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {
                if ($category['top']) {
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id'  => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );
                    }

                    // Level 1
                    $data['categories'][] = array(
                        'name'     => $category['name'],
                        'children' => $children_data,
                        'column'   => $category['column'] ? $category['column'] : 1,
                        'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }

        }

        $data['social_facebook'] = $this->config->get('config_social1');
        $data['social_pinterest'] = $this->config->get('config_social2');
        $data['social_instagram'] = $this->config->get('config_social3');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		if (version_compare(VERSION, '2.2', '>=')) {
			return ($this->load->view('common/amp_footer', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/amp_footer.tpl')) {
				return ($this->load->view($this->config->get('config_template') . '/template/common/amp_footer.tpl', $data));
			} else {
				return ($this->load->view('default/template/common/amp_footer.tpl', $data));
			}
		}

	}
	    public function getcustommenuLink($parent, $child = null) {
        if ($this->config->get('configcustommenu_custommenu')) {
            $item = empty($child) ? $parent : $child;

            switch ($item['custommenu_type']) {
                case 'category':
                    $route = 'product/category';

                    if (!empty($child)) {
                        $args = 'path=' . $parent['link'] . '_' . $item['link'];
                    } else {
                        $args = 'path='.$item['link'];
                    }
                    break;
                case 'product':
                    $route = 'product/product';
                    $args = 'product_id='.$item['link'];
                    break;
                case 'manufacturer':
                    $route = 'product/manufacturer/info';
                    $args = 'manufacturer_id='.$item['link'];
                    break;
                case 'information':
                    $route = 'information/information';
                    $args = 'information_id='.$item['link'];
                    break;
                default:
                    $tmp = explode('&', str_replace('index.php?route=', '', $item['link']));

                    if (!empty($tmp)) {
                        $route = $tmp[0];
                        unset($tmp[0]);
                        $args = (!empty($tmp)) ? implode('&', $tmp) : '';
                    }
                    else {
                        $route = $item['link'];
                        $args = '';
                    }

                    break;
            }

            $check = stripos($item['link'], 'http');
            $checkbase = strpos($item['link'], '/');
            if ( $check === 0 || $checkbase === 0 ) {
                $link = $item['link'];
            } else {
                $link = $this->url->link($route, $args);
            }
            return $link;
        }

}

}
