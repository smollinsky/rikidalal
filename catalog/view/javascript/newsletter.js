function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

$(document).ready(function(){
    $('#subcribe').click(function(){
        var email = $('#newsletter #input-newsletter').val();

        if(email == ''){
            var error = '';
            $("#input-newsletter").addClass('required');
        }

        if( !validateEmail(email)) {
            var error = 'Input email incorrect!';
        }

        if(error != null){
            $('#error-msg').html('');
            $('#error-msg').append('<b style=\"color:red\">' + error + '</b>');
        } else {
            var dataString = 'email='+ email;
            $.ajax({
                url: 'index.php?route=common/footer/addToNewsletter',
                type: 'post',
                data: dataString,
                success: function(html) {
                    $("#input-newsletter").removeClass('required');
                    $('#error-msg').append('<b style=\"color:green\">' + html + '</b>');
                    setTimeout (function(){
                        $('#error-msg').hide();
                        $('#newsletter #input-newsletter').val('');
                    }, 3000);

                }

            });
        }

    })
});