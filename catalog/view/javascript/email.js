function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

$(document).ready(function(){
    $('#subcribe').click(function(){
        var email = $('#input-newsletter').val();

        if(email == ''){
            var error = 'Введите email адрес!';
        }

        if( !validateEmail(email)) {
            var error = 'Введите правильный email адрес!';
        }

        if(error != null){
            $('#error-msg').html('');
            $('#error-msg').append('<b style=\"color:red\">' + error + '</b>');
        } else {

            var dataString = 'email='+ email;
            $.ajax({
                url: 'index.php?route=common/footer/addToNewsletter',
                type: 'post',
                data: dataString,
                success: function(html) {
                    $('#error-msg').append('<b style=\"color:green\">' + html + '</b>');
                }

            });
        }

    })
});