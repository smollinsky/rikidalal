<div class="fillter">
  <?php foreach ($filter_groups as $filter_group) { ?>
  <div class="wrapper" id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
    <p><?php echo $filter_group['name']; ?></p>
    <select name="filter[]">
      <option selected="selected" value=""> All </option>
      <?php foreach ($filter_group['filter'] as $filter) { ?>
      <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
      <option value="<?php echo $filter['filter_id']; ?>" selected="selected"><?php echo $filter['name']; ?></option>
      <?php } else { ?>
      <option value="<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></option>
      <?php } ?>
      <?php } ?>
    </select>
  </div>
  <?php } ?>
</div>

<script type="text/javascript"><!--
$('select[name^=\'filter\']').on('change', function() {
	filter = [];

  $('select[name^=\'filter\'] option:selected').each(function(element) {
    if (this.value != "") {
      filter.push(this.value);
    }
  });

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});
//--></script>
