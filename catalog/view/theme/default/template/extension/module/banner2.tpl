<section class="our_latest_block">
    <div class="container">
        <?php foreach ($banners as $banner) { ?>
        <div class="entity">
            <div class="__left">
                <h1 class="header"><?php echo $banner['title']; ?></h1>
                <p class="sub_header"><?php echo $banner['title2']; ?></p><a class="btn_effect" href="<?php echo $banner['link']; ?>"><span class="vert"></span><span class="horz"></span><span class="text"><?php echo $text_collections; ?></span></a>
            </div>
            <div class="__right">
                <div class="img" style="background-image:url('<?php echo $banner['image']; ?>')"></div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>