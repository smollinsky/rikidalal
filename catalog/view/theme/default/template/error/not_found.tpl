<?php echo $header; ?>
<section class="not_found_block">
  <div class="container">
    <div class="line">
      <div class="__left">
        <img src="catalog/view/theme/default/img/content/style1.png" srcset="catalog/view/theme/default/img/content/style1@2x.png 2x, catalog/view/theme/default/img/content/style1@3x.png 3x" style="max-width: 179px;">
      </div>
      <div class="__right">
        <h1 class="header">404</h1>
        <p class="sub_header"><?php echo $heading_title; ?></p>
        <p class="text"><?php echo $text_error; ?></p>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>
