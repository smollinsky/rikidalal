<?php echo $header; ?>
<div class="container">
  <div class="bread_crumbs_block">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>
<section class="partner_block">
  <div class="container">
    <h1 class="header"><?php echo $heading_title; ?></h1>
  </div>
  <?php echo $description; ?>
</section>

<?php echo $footer; ?>