<!doctype html>
<html ⚡>
<head>
  <meta charset="utf-8">
  <title><?php echo $meta_title; ?></title>
  <link rel="canonical" href="<?php echo $canonical; ?>" >
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <style amp-custom>
html {overflow-x:hidden}
body,html{height:auto}
body {
    margin:0;-webkit-text-size-adjust:100%;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;text-size-adjust:100%
}
body {
    font-family: 'Roboto', Arial;
}
a {
text-decoration:none;   

  </style>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-script" src="https://cdn.ampproject.org/v0/amp-script-0.1.js"></script>
<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
</head>
<body>
<div itemscope itemtype="http://schema.org/Product" class="amp_wrapper">
<div class="amp-header">
            <a href="<?php echo $home; ?>" class="amp-header_link">
                <amp-img src="<?php echo $logo; ?>" width="228" height="42" alt="an image" class="amp-logo -amp-element -amp-layout-fixed -amp-layout-size-defined -amp-layout" id="AMP_1" >
                 </amp-img>
            </a>         
        </div>
        <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php $breadcount = count($breadcrumbs) - 1; ?>
        <?php $i = 0; ?>
         <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
         <?php $i++; ?>
         <?php if ($key != $breadcount) { ?>
  <li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
    <a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
    <meta itemprop="position" content="<?php echo $i; ?>" />
  </li>
  <?php } else {?>
  <li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
   <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
    <meta itemprop="position" content="<?php echo $i; ?>" />
  </li>
    <?php } ?>  <?php } ?>
</ol>

  <h1 itemprop="name">Hubspot Test Form Page</h1>


  <h1 itemprop="name">Hubspot Test Form Page</h1>
 <amp-img src="https://unsplash.it/600/600" width=600px height=620px ></amp-img>

 
<amp-iframe width="300"
  height="500" layout="responsive" sandbox="allow-scripts allow-same-origin allow-forms" src="https://rikidalal2019.wpengine.com/test.html"></amp-iframe>


   
 <div class="amp-page_footer">
                <a href="<?php echo $base; ?>" class="amp-page_footer_link">
          <span><?php echo $name ?></span></a>
<span>© <?php echo date("Y"); ?></span>

            </div>   
</div>
</body>
</html>