<!doctype html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html amp dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
  <!-- ++++++++++++-->
  <!-- MAIN SCRIPTS-->
  <!-- ++++++++++++-->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
  <script src="catalog/view/theme/default/plugins/FormStyler/dist/jquery.formstyler.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
  <script src="catalog/view/theme/default/plugins/slick/slick/slick.min.js"></script>
  <!-- User customization-->
  <script src="catalog/view/theme/default/js/script.js"></script>
  <link href="catalog/view/theme/default/fonts/stylesheet.css" rel="stylesheet">
  <link href="catalog/view/theme/default/css/main.min.css" rel="stylesheet">
   <link href="catalog/view/theme/default/css/custom.css" rel="stylesheet">
  <link href="catalog/view/theme/default/css/custom-edit.css" rel="stylesheet">
  <link href="catalog/view/theme/default/plugins/FormStyler/dist/jquery.formstyler.css" rel="stylesheet">
  <link href="catalog/view/theme/default/plugins/FormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/plugins/slick/slick/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/plugins/slick/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/plugins/slick/slick/slick-theme.css">
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/plugins/slick/slick/slick.css">


<!--<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
  <?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?> flex">
<header>
  <div class="desktop__header">
    <div class="_top_header">
      <div class="container">
        <div class="line"><a class="req" href="/index.php?route=common/request"><?php echo $text_request; ?></a>
          <div class="social_link">
            <a class="_link" href="<?php echo $social_facebook; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="10" height="16">
                <path fill-rule="evenodd" fill="#BBB" d="M8.699,0.002 L6.780,-0.001 C4.624,-0.001 4.000,0.757 4.000,2.999 L4.000,5.999 L1.000,5.999 C0.833,5.999 1.000,5.529 1.000,5.699 L1.000,8.164 C1.000,8.334 0.833,7.999 1.000,7.999 L4.000,7.999 L4.000,14.999 C4.000,15.168 3.833,14.999 4.000,14.999 L6.050,14.999 C6.216,14.999 6.000,15.168 6.000,14.999 L6.000,7.999 L9.000,7.999 C9.167,7.999 8.909,8.334 8.909,8.164 L9.000,5.999 C9.000,5.918 9.056,6.057 9.000,5.999 C8.943,5.942 9.080,5.999 9.000,5.999 L6.000,5.999 L6.000,2.999 C6.000,2.305 6.078,2.593 6.000,1.999 L9.000,1.999 C9.166,1.999 9.000,2.168 9.000,1.999 L9.000,0.308 C9.000,0.139 8.865,0.002 8.699,0.002 Z"
                />
              </svg>
            </a>
            <a class="_link" href="<?php echo $social_pinterest; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M4.522,9.920 C4.150,11.985 3.697,13.965 2.354,14.999 C1.940,11.878 2.963,9.534 3.438,7.045 C2.628,5.599 3.536,2.685 5.244,3.404 C7.347,4.286 3.423,8.783 6.057,9.345 C8.807,9.932 9.930,4.282 8.225,2.445 C5.761,-0.207 1.053,2.384 1.632,6.182 C1.773,7.111 2.677,7.393 1.993,8.674 C0.416,8.303 -0.055,6.984 0.006,5.225 C0.104,2.344 2.445,0.328 4.793,0.050 C7.762,-0.303 10.549,1.206 10.934,4.170 C11.367,7.516 9.594,11.139 6.418,10.878 C5.558,10.807 5.196,10.355 4.522,9.920 Z"
                />
              </svg>
            </a>
            <a class="_link" href="<?php echo $social_instagram; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
                />
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="_bottom_header">
        <?php if ($logo) { ?>
        <?php if ($home == $og_url) { ?>
        <a class="logo" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" srcset="catalog/view/theme/default/img/content/logo-riki-dalal-white@2x.png 2x,catalog/view/theme/default/img/content/logo-riki-dalal-white@3x.png 3x" /></a>
        <?php } else { ?>
        <a class="logo" href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" srcset="catalog/view/theme/default/img/content/logo-riki-dalal-white@2x.png 2x,catalog/view/theme/default/img/content/logo-riki-dalal-white@3x.png 3x" /></a>
        <?php } ?>
        <?php } else { ?>
        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
        <?php } ?>
        <?php if ($categories) { ?>
        <menu class="menu">
          <?php foreach ($categories as $category) { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
        </menu>
        <?php } ?>
        <a class="btn_simple" href="#"><?php echo $text_contactus; ?></a>
      </div>
    </div>
  </div>
  <div class="mobile__header">
    <div class="_fixed_part">
    <div class="_top_part">
      <div class="container">
        <div class="line">
          <div class="sendwich"><span class="sendwich-btn"></span>
          </div>
          <div class="social_link">
            <a class="_link" href="<?php echo $social_facebook; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="10" height="16">
                <path fill-rule="evenodd" fill="#BBB" d="M8.699,0.002 L6.780,-0.001 C4.624,-0.001 4.000,0.757 4.000,2.999 L4.000,5.999 L1.000,5.999 C0.833,5.999 1.000,5.529 1.000,5.699 L1.000,8.164 C1.000,8.334 0.833,7.999 1.000,7.999 L4.000,7.999 L4.000,14.999 C4.000,15.168 3.833,14.999 4.000,14.999 L6.050,14.999 C6.216,14.999 6.000,15.168 6.000,14.999 L6.000,7.999 L9.000,7.999 C9.167,7.999 8.909,8.334 8.909,8.164 L9.000,5.999 C9.000,5.918 9.056,6.057 9.000,5.999 C8.943,5.942 9.080,5.999 9.000,5.999 L6.000,5.999 L6.000,2.999 C6.000,2.305 6.078,2.593 6.000,1.999 L9.000,1.999 C9.166,1.999 9.000,2.168 9.000,1.999 L9.000,0.308 C9.000,0.139 8.865,0.002 8.699,0.002 Z"
                />
              </svg>
            </a>
            <a class="_link" href="<?php echo $social_pinterest; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M4.522,9.920 C4.150,11.985 3.697,13.965 2.354,14.999 C1.940,11.878 2.963,9.534 3.438,7.045 C2.628,5.599 3.536,2.685 5.244,3.404 C7.347,4.286 3.423,8.783 6.057,9.345 C8.807,9.932 9.930,4.282 8.225,2.445 C5.761,-0.207 1.053,2.384 1.632,6.182 C1.773,7.111 2.677,7.393 1.993,8.674 C0.416,8.303 -0.055,6.984 0.006,5.225 C0.104,2.344 2.445,0.328 4.793,0.050 C7.762,-0.303 10.549,1.206 10.934,4.170 C11.367,7.516 9.594,11.139 6.418,10.878 C5.558,10.807 5.196,10.355 4.522,9.920 Z"
                />
              </svg>
            </a>
            <a class="_link" href="<?php echo $social_instagram; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
                />
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php if ($categories) { ?>
    <menu class="menu">
      <?php foreach ($categories as $category) { ?>
      <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
      <?php } ?>
      <li><a class="btn_simple" href="#">Contact us</a>
      </li>
    </menu>
    <?php } ?>
  </div>
    <div class="_bottom_part">
      <img class="mobile_logo" src="catalog/view/theme/default/img/minified-svg/logo_dalal_mobile.svg" alt="logo">
    </div>
  </div>
</header>
