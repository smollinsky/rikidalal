<?php echo $header; ?>
<div class="container">
  <div class="bread_crumbs_block">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>
<section class="article_block">
  <div class="container">
    <h1 class="header"><?php echo $heading_title; ?></h1>
    <div class="content">
      <div class="line">
        <?php echo $description; ?>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript"><!--
$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#description').find('a>img').each(function(){
    $(this).parent().addClass('gallery');
  });
  $('#description').magnificPopup({
    delegate: 'a.gallery',
    type: 'image',
    gallery: {
        enabled: true
    }
  });

  gotoReview = function() {
    offset = $('#form-review').offset();
    $('html, body').animate({ scrollTop: offset.top-20 }, 'slow');
  }
  gotoReviewWrite = function() {
    offset = $('#form-review h2').offset();
    $('html, body').animate({ scrollTop: offset.top-20 }, 'slow');
  }
  
});
--></script>
<?php echo $footer; ?>