<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_menu']   = 'Menu';
$_['text_followus']   = 'Follow us';
$_['text_newsletter']   = 'Newsletter';
$_['text_newsletter_text']   = 'Enter your e-mail';
$_['text_subcribe']   = 'Subcribe';
$_['text_error_subcribe']   = 'You have already subcribed newsletter!';
$_['text_success_subcribe']   = 'You have successfuly subcribe newsletter!';
$_['text_powered']      = 'Copyright %s &copy; %s Riki Dalal, Inc. All Rights Reserved – Design by <a href="#">ItHoot</a>';
