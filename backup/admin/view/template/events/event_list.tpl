<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default" onclick="$('#form-event').attr('action', '<?php echo $copy; ?>').submit()"><i class="fa fa-copy"></i></button>
               <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-event').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-event">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                <td class="text-center"><?php echo $column_image; ?></td>
                                <td class="text-center">Country, City</td>
                                <td class="text-center">Store name</td>
                                <td class="text-center">Store_address</td>
                                <td>Date event</td>
                                <td class="text-left"><?php echo $column_status; ?></td>
                                <td class="text-right"><?php echo $column_action; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($events) { ?>
                            <?php foreach ($events as $event) { ?>
                            <tr>
                                <td class="text-center"><?php if (in_array($event['event_id'], $selected)) { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $event['event_id']; ?>" checked="checked" />
                                    <?php } else { ?>
                                    <input type="checkbox" name="selected[]" value="<?php echo $event['event_id']; ?>" />
                                    <?php } ?></td>
                                <td class="text-center"><?php if ($event['image']) { ?>
                                    <img src="<?php echo $event['image']; ?>" alt="<?php echo $event['name']; ?>" class="img-thumbnail" />
                                    <?php } else { ?>
                                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                    <?php } ?></td>
                                <td class="text-left"><?php echo $event['country']; ?>, <?php echo $event['city']; ?></td>
                                <td class="text-left"><?php echo $event['store_name']; ?></td>
                                <td class="text-left"><?php echo $event['store_address']; ?></td>
                                <td class="text-left"><?php echo $event['date_from']; ?> - <?php echo $event['date_to']; ?></td>
                                <td class="text-left"><?php echo $event['status']; ?></td>
                                <td class="text-right">
                                      <a href="<?php echo $event['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                </div>
            </div>
        </div>
    </div>


</div>
<?php echo $footer; ?>