<?php

$_['text_botique']                    = 'Flagship Boutiques';
$_['text_botique1']                    = 'The ultimate Riki Dalal experience. Our very own bridal shop featuring our entire set of collection. Our expert staff and professional stylists will be available for any questions you may have.';
$_['text_botique2']                    = 'Worldwide Boutiques';
$_['text_botique3']                    = 'Explore our collection of gowns in a variety of locations across the globe. Each boutique will feature our collection of gowns on display. Find the nearest boutique in your area and get the best service possible.';
