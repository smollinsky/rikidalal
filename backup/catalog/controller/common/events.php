<?php
class ControllerCommonEvents extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical');
        }
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_event'),
            'href' => $this->url->link('common/events')
        );

        $this->load->language('common/stores');

        $data['text_botique_search1'] = $this->language->get('text_botique_search1');

        $this->load->model('catalog/events');

        $this->load->model('tool/image');

        $data['events'] = array();

        $results = $this->model_catalog_events->getEvents();

        foreach ($results as $result) {
            if ($result['image']) {
                $image = 'image/'.$result['image'];
            } else {
                $image = false;
            }

            $data['events'][] = array(
                'event_id'  => $result['event_id'],
                'thumb'       => $image,
                'country'        => $result['country'],
                'city' =>           $result['city'],
                'store_name' =>           $result['store_name'],
                'store_address' =>           $result['store_address'],
                'date_from'  => date($this->language->get('date_format_short'), strtotime($result['date_from'])),
                'date_to'  => date($this->language->get('date_format_short'), strtotime($result['date_to'])),
                'areakm' =>           $result['areakm'],
                'status_event' =>           $result['status_event']
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/events', $data));
    }

    public function getAll(){

            $data['event_search0'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search0'] = array(
                    'event_id' => $result['event_id'],
                    'country' => $result['country'],
                    'city' => $result['city'],
                    'store_address' => $result['store_address'],
                    'store_name' => $result['store_name'],
                    'areakm' => $result['areakm'],
                    'image'         => $result['image'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));
    }

    public function getToday(){

        if (isset($this->request->get['date_from'])) {
            $date_from = $this->request->get['date_from'];

            $data['event_search1'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND date_from = '" . $date_from . "'");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search1'] = array(
                	'event_id' => $result['event_id'],
                    'country' => $result['country'],
                    'city' => $result['city'],
                    'store_address' => $result['store_address'],
                    'store_name' => $result['store_name'],
                    'areakm' => $result['areakm'],
                    'image'         => $result['image'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));
        }
    }

    public function get7day(){

            $data['event_search2'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND DATE(date_from) BETWEEN DATE(NOW()) AND DATE_ADD(DATE(NOW()), INTERVAL 7 DAY)");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search2'] = array(
                    'event_id' => $result['event_id'],
                    'country' => $result['country'],
                    'city' => $result['city'],
                    'store_address' => $result['store_address'],
                    'store_name' => $result['store_name'],
                    'areakm' => $result['areakm'],
                    'image'         => $result['image'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));        
    }

    public function get30day(){

            $data['event_search3'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND DATE(date_from) BETWEEN DATE(NOW()) AND DATE_ADD(DATE(NOW()), INTERVAL 30 DAY)");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search3'] = array(
                    'event_id' => $result['event_id'],
                    'country' => $result['country'],
                    'city' => $result['city'],
                    'store_address' => $result['store_address'],
                    'store_name' => $result['store_name'],
                    'areakm' => $result['areakm'],
                    'image'         => $result['image'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));        

    }

    public function getBetweenDays(){

        if (isset($this->request->get['date_from']) and isset($this->request->get['date_to'])) {
            $date_from = $this->request->get['date_from'];
            $date_to = $this->request->get['date_to'];

            $data['event_search5'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND BETWEEN DATE(date_from) AND DATE(date_to)");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search5'] = array(
                    'event_id' => $result['event_id'],
                    'country' => $result['country'],
                    'city' => $result['city'],
                    'store_address' => $result['store_address'],
                    'store_name' => $result['store_name'],
                    'areakm' => $result['areakm'],
                    'image'         => $result['image'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));
        }
    }

    public function autocomplete_event(){
        if(!empty($_POST["search"])){

            $search = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["search"]))));

            $db_search = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND city LIKE '%$search%'");

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();           

            foreach($db_search->rows as $result){             
                echo "\n<li><span class='value'>".$result["city"]."</span> <span></span></li>";             
            }
        }
    }

    public function event_result(){
        if (isset($this->request->get['city'])) {
            $city = $this->request->get['city'];

            $data['event_search'] = array();

            $data['event_search'] = $this->db->query("SELECT e.*, ed.* FROM oc_events e LEFT JOIN oc_event_description ed ON (e.event_id = ed.event_id) WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND city = '" . $city . "'");

            foreach ($data['event_search']->rows as $result) {
                $data['event_search1'] = array(
                    'event_id' => $result['event_id'],
                    'country'        => $result['country'],
                    'city'        => $result['city'],
                    'store_address'     => nl2br($result['store_address']),
                    'store_name'   => $result['store_name'],
                    'image'         => $result['image'],
                    'date_from'        => $result['date_from'],
                    'date_to'     => $result['date_to'],
                    'status'     => $result['status'],
                    'status_event'     => $result['status_event']
                );
            }

            $this->response->setOutput($this->load->view('common/event_result', $data));
        }

    }

}
