<?php
class ControllerCommonSendmail extends Controller {
 public function index() {
 
 }
 public function sendmailCustom (){

 $json = array();
 $json['name'] = $this->request->post['name'];
 $json['email'] = $this->request->post['email'];
 $json['enquiry'] = $this->request->post['enquiry'];
if ($this->request->server['REQUEST_METHOD'] == 'POST') {
 $mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
            $mail->setReplyTo($json['email']);
			$mail->setSender(html_entity_decode($json['name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $json['name']), ENT_QUOTES, 'UTF-8'));
			$mail->setText($json['enquiry'] );
			$mail->send();

		}

 $this->response->addHeader('Content-Type: application/json');
 $this->response->setOutput(json_encode($json));
 }
}