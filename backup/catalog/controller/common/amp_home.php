<?php

class ControllerCommonAmpHome extends Controller {

	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));


		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}
        if (is_file(DIR_IMAGE . $this->config->get('config_logo1'))) {
            $data['logo1'] = $server . 'image/' . $this->config->get('config_logo1');
        } else {
            $data['logo1'] = '';
        }

        // Menu
        $this->load->model('design/custommenu');
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        if ($this->config->get('configcustommenu_custommenu')) {
            $custommenus = $this->model_design_custommenu->getcustommenus();
            $custommenu_child = $this->model_design_custommenu->getChildcustommenus();

            foreach($custommenus as $id => $custommenu) {
                $children_data = array();

                foreach($custommenu_child as $child_id => $child_custommenu) {
                    if (($custommenu['custommenu_id'] != $child_custommenu['custommenu_id']) or !is_numeric($child_id)) {
                        continue;
                    }

                    $child_name = '';

                    if (($custommenu['custommenu_type'] == 'category') and ($child_custommenu['custommenu_type'] == 'category')){
                        $filter_data = array(
                            'filter_category_id'  => $child_custommenu['link'],
                            'filter_sub_category' => true
                        );

                        $child_name = ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '');
                    }

                    $children_data[] = array(
                        'name' => $child_custommenu['name'] . $child_name,
                        'href' => $this->getcustommenuLink($custommenu, $child_custommenu)
                    );
                }

                $data['categories'][] = array(
                    'name'     => $custommenu['name'] ,
                    'children' => $children_data,
                    'column'   => $custommenu['columns'] ? $custommenu['columns'] : 1,
                    'href'     => $this->getcustommenuLink($custommenu)
                );
            }

        } else {

            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {
                if ($category['top']) {
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id'  => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );
                    }

                    // Level 1
                    $data['categories'][] = array(
                        'name'     => $category['name'],
                        'children' => $children_data,
                        'column'   => $category['column'] ? $category['column'] : 1,
                        'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }

        }

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $data['text_collection'] = $this->language->get('text_collection');

        $this->load->language('extension/module/slideshow');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_text'] = $this->language->get('heading_text');

        $data['banners_main'] = array();

        $results_main = $this->model_design_banner->getBanner(7);

        foreach ($results_main as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners_main'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
                );
            }
        }

        $data['banners_about'] = array();

        $results_about = $this->model_design_banner->getBanner(6);

        foreach ($results_about as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners_about'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
                );
            }
        }

        $data['banners_our'] = array();

        $results_our = $this->model_design_banner->getBanner(9);

        foreach ($results_our as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners_our'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
                );
            }
        }
        $this->load->language('extension/module/carousel');

        $data['heading_title_clients'] = $this->language->get('heading_title_clients');
        $data['banners_clients'] = array();

        $results_clients = $this->model_design_banner->getBanner(8);

        foreach ($results_clients as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners_clients'][] = array(
                    'title' => $result['title'],
                    'title2' => $result['title2'],
                    'link'  => $result['link'],
                    'image' => 'image/'.$result['image']
                );
            }
        }


        $this->load->language('extension/module/featured');

        $data['heading_title_amp'] = $this->language->get('heading_title_amp');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $query_featured = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE code = 'featured'");

        $featureds = json_decode($query_featured->row['setting'], true);

        $data['products'] = array();

        if (!empty($featureds['product'])) {
            $products = array_slice($featureds['product'], 0, 4);

            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info) {
                    if ($product_info['image']) {
                        $image = "image/".$product_info['image'];
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    $data['products'][] = array(
                        'product_id'  => $product_info['product_id'],
                        'thumb'       => $image,
                        'name'        => $product_info['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'href'        => $this->url->link('product/amp_product', 'product_id=' . $product_info['product_id'])
                    );
                }
            }
        }

        $this->load->language('extension/module/imgcategory');

        $data['heading_title_img'] = $this->language->get('heading_title_img');
        $data['heading_texts_img'] = $this->language->get('heading_texts_img');

        $this->load->model('catalog/category');

        $this->load->model('tool/image');

        $data['categoriess'] = array();

        $query_category = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE code = 'imgcategory'");

        $categoriess = json_decode($query_category->row['setting'], true);

        $results_category = $this->model_catalog_category->getCategories($categoriess['category_id']);

        foreach ($results_category as $result) {

            if ($result['image']) {
                $image = "image/".$result['image'];
            } else {
                $image = 'placeholder.png';
            }


            $data['categoriess'][] = array (
                'href' 	=> $this->url->link('product/category', 'path=' . $result['category_id']),
                'thumb'	=> $image,
                'name' 	=> $result['name'],
                'column' 	=> $result['column'],
            );
        }

        $this->load->language('extension/module/instagram_rdr');
        $this->load->model('tool/image');
        $this->load->model('extension/module/instagram_rdr');

        $data['text_follow'] = $this->language->get('text_follow');
        $data['text_posts'] = $this->language->get('text_posts');
        $data['text_followed'] = $this->language->get('text_followed');
        $data['text_follows'] = $this->language->get('text_follows');

        $query_instagram = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE code = 'instagram_rdr'");

        $instagram = json_decode($query_instagram->row['setting'], true);

        $photos = $this->model_extension_module_instagram_rdr->getPhotos($instagram['token'], $instagram['amount']);

        if(!empty($photos)) {
            $data['title'] = isset($instagram['title'][$this->config->get('config_language_id')]) ? $instagram['title'][$this->config->get('config_language_id')] : '';

            $data['photos'] = array();
            foreach ($photos as $photo) {
                $image = "image/" . $photo['image'];
                $data['photos'][] = array(
                    'createdDate' => date($this->language->get('date_format_short'), strtotime($photo['createdTime'])),
                    'type' => $photo['type'],
                    'link' => $photo['link'],
                    'image' => $image,
                    'alt' => strip_tags($photo['caption']),
                    'imageUrl' => $photo['imageUrl'],
                    'caption' => nl2br($photo['caption'])
                );
            }

            $data['account'] = null;
            if ($instagram['account_info']) {
                $account = $this->model_extension_module_instagram_rdr->getAccount($instagram['token']);
                if (!empty($account)) {
                    $image = $this->model_tool_image->resize($account['image'], 70, 70);
                    $data['account'] = array(
                        'username' => $account['username'],
                        'link' => $account['url'],
                    );
                }
            }
        }

		$data['home'] = $this->url->link('common/home');
		$data['base'] = $server;

        $this->load->language('common/home');

        $data['text_search1'] = $this->language->get('text_search1');
        $data['text_search2'] = $this->language->get('text_search2');
        $data['text_search3'] = $this->language->get('text_search3');
        $data['text_search4'] = $this->language->get('text_search4');
        $data['text_search5'] = $this->language->get('text_search5');
        $data['text_contact1'] = $this->language->get('text_contact1');
        $data['text_contact2'] = $this->language->get('text_contact2');
        $data['text_contact3'] = $this->language->get('text_contact3');
        $data['text_contact4'] = $this->language->get('text_contact4');
        $data['text_contact5'] = $this->language->get('text_contact5');
        $data['text_contact6'] = $this->language->get('text_contact6');

		$data['social_facebook'] = $this->config->get('config_social1');
        $data['social_pinterest'] = $this->config->get('config_social2');
        $data['social_instagram'] = $this->config->get('config_social3');

        
		$data['amp_footer'] = $this->load->controller('common/amp_footer');
		// $data['amp_header'] = $this->load->controller('common/amp_header');

		if (version_compare(VERSION, '2.2', '>=')) {
			$this->response->setOutput($this->load->view('common/amp_home', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/amp_home.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/amp_home.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/common/amp_home.tpl', $data));
			}
		}
	}

    public function sendmail_amp() {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: https://' . $_SERVER['HTTP_HOST']);
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Expose-Headers: AMP-Access-Control-Allow-Source-Origin');
        header('AMP-Access-Control-Allow-Source-Origin: https://' . $_SERVER['HTTP_HOST']);

        $data = array();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $name = strip_tags(trim($_POST["contact-name"]));
            $name = str_replace(array("\r", "\n"), array(" ", " "), $name);
            $email = trim($_POST["contact-email"]);
            $message = trim($_POST["contact-message"]);

            if (empty($name) OR empty($email)) {
                http_response_code(400);
                exit;
            }

            $recipient = $this->config->get('config_email');
            $subject = "Email from guest: $name";

            $email_content = "From: $name\n\n";
            $email_content .= "Email: $email\n\n";
            $email_content .= "Message:\n\n $message";

            $email_headers = "From: $name <$email>";

            if (mail($recipient, $subject, $email_content, $email_headers)) {
                $data = array();
                $data['name'] = $_POST['contact-name'];
                echo json_encode($data);

            } else {
                http_response_code(500);
                exit;
            }

        } else {
            http_response_code(403);
            exit;
        }

    }

    public function getcustommenuLink($parent, $child = null) {
        if ($this->config->get('configcustommenu_custommenu')) {
            $item = empty($child) ? $parent : $child;

            switch ($item['custommenu_type']) {
                case 'category':
                    $route = 'product/category';

                    if (!empty($child)) {
                        $args = 'path=' . $parent['link'] . '_' . $item['link'];
                    } else {
                        $args = 'path='.$item['link'];
                    }
                    break;
                case 'product':
                    $route = 'product/product';
                    $args = 'product_id='.$item['link'];
                    break;
                case 'manufacturer':
                    $route = 'product/manufacturer/info';
                    $args = 'manufacturer_id='.$item['link'];
                    break;
                case 'information':
                    $route = 'information/information';
                    $args = 'information_id='.$item['link'];
                    break;
                default:
                    $tmp = explode('&', str_replace('index.php?route=', '', $item['link']));

                    if (!empty($tmp)) {
                        $route = $tmp[0];
                        unset($tmp[0]);
                        $args = (!empty($tmp)) ? implode('&', $tmp) : '';
                    }
                    else {
                        $route = $item['link'];
                        $args = '';
                    }

                    break;
            }

            $check = stripos($item['link'], 'http');
            $checkbase = strpos($item['link'], '/');
            if ( $check === 0 || $checkbase === 0 ) {
                $link = $item['link'];
            } else {
                $link = $this->url->link($route, $args);
            }
            return $link;
        }

    }

}
