$('.btn_simple').click(function() {
    $.ajax({
        url: 'index.php?route=common/home/send_form',
        type: 'post',
        data: $('#contact input, #contact textarea'),
        dataType: 'json',
        success: function(json) {
            if(json['error1']){
                $('input[name="name"]').addClass('required');
            } else {
                $('input[name="name"]').removeClass('required');
            }
            if(json['error2']){
                $('input[name="email"]').addClass('required');
            } else {
                $('input[name="email"]').removeClass('required');
            }
            if(json['error3']){
                $('textarea[name="message"]').addClass('required');
            } else {
                $('textarea[name="message"]').removeClass('required');
            }
            if(json['success']){
                $('.btn_simple').text('Success!');
                setTimeout (function(){
                    $('.btn_simple').text('Send');
                    $('#contact input, #contact textarea').val('');
                }, 3000);
            }

        }
    });
});