<section class="about_block">
  <div class="container">
    <h3 class="header"><?php echo $heading_title; ?></h3>
    <p class="sub_header"><?php echo $heading_text; ?></p>
    <div class="wrapper_slider">
      <div class="about_slider">
        <?php foreach ($banners as $banner) { ?>
        <div class="slider__item">
          <p class="had"><?php echo $banner['title']; ?></p>
          <p class="text"><?php echo $banner['title2']; ?></p>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
    // slider about
    $(".about_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true
    });
  });
</script>