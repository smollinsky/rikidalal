<section class="insta_block">
    <h3 class="header"><?= $title ?></h3><?php if($account) { ?><a class="sub_header" href="<?= $account['link'] ?>">@<?= $account['username'] ?></a><?php } ?>
    <div class="insta_slider">
        <?php foreach ($photos as $photo) { ?>
        <a class="slider__item" href="<?= $photo['link']; ?>" target="_blank">
            <div class="img" style="background-image:url('<?= $photo['image']; ?>')">
                <?php if($photo['type'] == 'video') { ?>
                <span class="video">
                            <i class="fa fa-play"></i>
                        </span>
                <?php } ?>
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
                    <path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
                    />
                </svg>
            </div>
        </a>
        <?php } ?>
    </div>
</section>
<script>
    // insta about
    $(".insta_slider").slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
</script>