<section class="our_client_block">
  <div class="container">
    <div class="entity">
      <h1 class="header"><?php echo $heading_title; ?></h1>
      <div class="client_slider">
        <?php foreach ($banners as $banner) { ?>
        <div class="client__item">
          <p class="sub_header"><?php echo $banner['title2']; ?></p>
          <div class="profile">
            <div class="img" style="background-image:url('<?php echo $banner['image']; ?>')"></div>
            <div class="content">
              <p class="name"><?php echo $banner['title']; ?></p>
              <p class="date">January 2019</p>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function() {
       // client about
    $(".client_slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true
    });
  });
</script>