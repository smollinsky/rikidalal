<section class="dress_style_blcok">
    <div class="container">
        <h3 class="header"><?php echo $heading_title; ?></h3><a class="sub_header" href="#"><?php echo $heading_texts; ?></a>
        <div class="trend_list">
            <?php foreach ($categories as $category) { ?>
            <?php if($category['top']) { ?>
            <div class="wrapper">
                <a class="trend__item" href="<?php echo $category['href']; ?>">
                    <img src="<?php echo $category['thumb']; ?>" srcset="<?php echo $category['thumb']; ?>, <?php echo $category['thumb']; ?>" style="max-width: <?php echo $category['column']; ?>px;">
                    <p><?php echo $category['name']; ?></p>
                </a>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
    // dress_list
    $(".dress_style_blcok .trend_list").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    arrows: true,
                    prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
                    nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
                    dots: true,
                    autoplay: true
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
                    nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
                    dots: true,
                    autoplay: true
                }
            },
            {
                breakpoint: 374,
                settings: {
                    slidesToShow: 1,
                    prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
                    nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
                    dots: true,
                    arrows: true,
                }
            }
        ]
    });
    });
</script>