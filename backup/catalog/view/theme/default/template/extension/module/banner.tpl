<?php foreach ($banners as $banner) { ?>
<section class="banner_block" id="banner<?php echo $module; ?>" style="background-image:url('<?php echo $banner['image']; ?>')">
  <div class="container">
    <div class="entity">
      <h1 class="header"><?php echo $banner['title']; ?></h1>
      <p class="sub_header"><?php echo $banner['title2']; ?></p><a class="btn_effect" href="<?php echo $banner['link']; ?>"><span class="vert"></span><span class="horz"></span><span class="text"><?php echo $text_collection; ?></span></a>
    </div>
  </div>
</section>
<?php } ?>
