<?php echo $header; ?>
<div class="container">
  <div class="bread_crumbs_block">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>
<?php if (!$products) { ?>
<section class="couture_block">
  <div class="container">
    <?php if ($thumb || $description) { ?>
    <h3 class="header"><?php echo $heading_title; ?></h3>
    <?php if ($description) { ?>
    <h3 class="sub_header"><?php echo $description; ?></h3>
    <?php } ?><?php } ?>
    <?php if ($categories) { ?>
    <div class="trend_list">
      <?php foreach ($categories as $category) { ?>
      <div class="wrapper">
        <a class="trend__item" href="<?php echo $category['href']; ?>" style="background-image:url('image/<?php echo $category['image']; ?>')">
          <div class="_hover">
            <p class="had"><?php echo $category['name']; ?></p>
          </div>
        </a>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</section>
<?php } ?>

<?php if ($products) { ?>
<div class="catalog_block">
  <div class="container">
    <h3 class="header"><?php echo $heading_title; ?></h3>
    <div class="mobile_filter">
    <div class="_left">
        <p><?php echo $text_items; ?> <span>(<?php echo $results; ?>)</span>
        </p>
    </div>
    <div class="_right">
        <p><?php echo $text_filter; ?></p>
        <svg class="cross" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="15px">
            <path fill-rule="evenodd" fill="rgb(187, 187, 187)" d="M14.822,1.033 L8.356,7.499 L14.822,13.965 C15.059,14.202 15.059,14.586 14.822,14.822 C14.704,14.940 14.549,15.000 14.394,15.000 C14.238,15.000 14.083,14.940 13.965,14.822 L7.500,8.356 L1.034,14.822 C0.916,14.940 0.761,15.000 0.606,15.000 C0.450,15.000 0.295,14.940 0.177,14.822 C-0.060,14.586 -0.060,14.202 0.177,13.965 L6.643,7.499 L0.177,1.033 C-0.060,0.797 -0.060,0.413 0.177,0.177 C0.414,-0.060 0.797,-0.060 1.034,0.177 L7.499,6.642 L13.965,0.176 C14.202,-0.060 14.585,-0.060 14.822,0.176 C15.059,0.413 15.059,0.797 14.822,1.033 Z"
            />
        </svg>
        <svg class="op" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
            <path fill-rule="evenodd" fill="rgb(187, 187, 187)" d="M20.000,2.688 C20.000,0.922 14.969,-0.001 10.000,-0.001 C5.031,-0.001 -0.000,0.922 -0.000,2.688 C-0.000,3.088 0.260,3.444 0.718,3.756 L7.600,12.798 L7.600,19.530 C7.600,19.694 7.702,19.842 7.861,19.906 L8.135,20.000 L12.400,15.905 L12.400,12.799 L19.274,3.766 L19.274,3.761 C19.736,3.448 20.000,3.090 20.000,2.688 ZM10.000,0.767 C15.616,0.767 19.200,1.904 19.200,2.688 C19.200,2.850 19.038,3.027 18.746,3.206 L18.726,3.203 L18.732,3.214 C17.598,3.898 14.438,4.608 10.000,4.608 C5.558,4.608 2.396,3.896 1.264,3.212 L1.271,3.201 L1.106,3.107 C0.910,2.962 0.800,2.820 0.800,2.688 C0.800,1.904 4.384,0.767 10.000,0.767 ZM11.600,15.587 L8.400,18.660 L8.400,13.058 L11.600,13.058 L11.600,15.587 ZM11.798,12.289 L8.203,12.289 L2.230,4.442 C4.160,5.059 7.090,5.376 10.000,5.376 C12.910,5.376 15.840,5.059 17.770,4.442 L11.798,12.289 Z"
            />
        </svg>
    </div>
</div>
    <?php echo $content_top; ?>
    <div class="trend_list">
      <?php foreach ($products as $product) { ?>
      <div class="wrapper">
        <a class="trend__item" href="<?php echo $product['href']; ?>" style="background-image:url('<?php echo $product['thumb']; ?>')">
          <div class="like" data-id="<?php echo $product['product_id']?>" >
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="20px">
              <path fill-rule="evenodd" fill="rgb(197, 166, 114)" d="M21.967,5.962 C21.646,2.505 19.143,-0.003 16.011,-0.003 C13.924,-0.003 12.014,1.095 10.939,2.854 C9.873,1.072 8.041,-0.003 5.988,-0.003 C2.856,-0.003 0.353,2.505 0.032,5.962 C0.007,6.114 -0.097,6.918 0.219,8.229 C0.675,10.119 1.729,11.838 3.266,13.200 L10.933,20.002 L18.732,13.200 C20.269,11.838 21.323,10.119 21.779,8.229 C22.096,6.918 21.992,6.115 21.967,5.962 Z"
              />
            </svg>
            <p class="count"><?php echo $product['count_like'] ?></p>
          </div>
          <div class="_hover">
            <p class="had"><?php echo $product['name']; ?></p>
            <p class="text"><?php echo $product['description']; ?></p>
          </div>
        </a>
      </div>
    <?php } ?>
    </div>
    <div class="row hidden" style="display:none;">
      <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
    <div class="text_area">
      <?php echo $description; ?>
    </div>
  </div>
</div>
<!-- contact form start -->
<section class="contact_block">
  <div class="__left"></div>
  <div class="__right">
    <div class="entity">
      <h1 class="header"><?php echo $text_contact1; ?></h1>
      <p class="sub_header"><?php echo $text_contact2; ?></p>
      <form id="contact">
        <div class="line">
          <input type="text" placeholder="<?php echo $text_contact3; ?>" name="name">
          <input type="text" placeholder="<?php echo $text_contact4; ?>*" name="email">
        </div>
        <textarea placeholder="<?php echo $text_contact5; ?>" name="message"></textarea>
        <div class="btn_simple"><?php echo $text_contact6; ?></div>
      </form>
    </div>
  </div>
</section>
<!-- contact form end -->
<?php } ?>
<div class="container">
<?php if (!$categories && !$products) { ?>
<p><?php echo $text_empty; ?></p>
<br><br>
<?php } ?>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/contact.js"></script>
<script>
  $(document).ready(function() {
    $(".like").bind("click", function(e) {
      e.preventDefault();
      var link = $(this);
      var id = link.data('id');
      $.ajax({
        url: "index.php?route=product/category/like",
        type: "POST",
        data: {id: id},
        success: function(data) {        	
            $('.count',link).html(data);
        }
      });
    });
    var link = $('.like');
    var id = link.data('id');
    if ($.cookie('like'+id)) { $('.trend__item .like').addClass('active'); }
  });
</script>

<script>
  $(document).ready(function() {
    // trend_list
    $(".couture_block .trend_list").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            arrows: true,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            autoplay: true,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows: true,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            autoplay: true
          }
        },
        {
          breakpoint: 374,
          settings: {
            slidesToShow: 1,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            arrows: true,
          }
        }
      ]
    });
  });
  </script>
<?php echo $footer; ?>
