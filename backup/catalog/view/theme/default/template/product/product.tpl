<?php echo $header; ?>
<div class="container">
  <div class="bread_crumbs_block">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>
<style>
  .bread_crumbs_block a:last-child{
    font-size: 16px;
    font-weight: 300;
    color: #e5d4b8;
  }
  .bread_crumbs_block a:last-child:after{
    content: none;
  }
</style>

<section class="product_block">
  <div class="container">
    <div class="line">
      <div class="_left">
        <?php if ($thumb || $images) { ?>
        <div class="display_slider">
          <?php if ($thumb) { ?>
          <div class="img" style="background-image:url('<?php echo $thumb; ?>')"></div>
          <?php } ?>
          <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <div class="img" style="background-image:url('<?php echo $image['thumb']; ?>')"></div>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="nav_slider">
          <?php if ($thumb) { ?>
          <div class="wrapper">
            <div class="img" style="background-image:url('<?php echo $thumb; ?>')"></div>
          </div>
          <?php } ?>
          <?php if ($images) { ?>
          <?php foreach ($images as $image) { ?>
          <div class="wrapper">
            <div class="img" style="background-image:url('<?php echo $image['thumb']; ?>')"></div>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      <div class="_right">
        <div class="line">
          <?php if (!empty($previous_name)) { ?>
          <a class="prev" href="<? echo $previous_url; ?>">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="9px" height="16px">
              <path fill-rule="evenodd" fill="rgb(223, 223, 223)" d="M7.845,14.846 C8.052,14.639 8.052,14.303 7.845,14.096 L1.262,7.507 L7.845,0.904 C8.052,0.697 8.052,0.361 7.845,0.155 C7.639,-0.052 7.304,-0.052 7.098,0.155 L0.155,7.119 C0.051,7.222 -0.000,7.352 -0.000,7.494 C-0.000,7.623 0.051,7.765 0.155,7.868 L7.098,14.833 C7.304,15.052 7.639,15.052 7.845,14.846 Z"
              />
            </svg>
            <p><?php echo $text_prev; ?></p>
          </a>
          <?php } ?>
          <?php if (!empty($next_name)) { ?>
          <a class="next" href="<? echo $next_url; ?>">
            <p><?php echo $text_next; ?></p>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8px" height="15px">
              <path fill-rule="evenodd" fill="rgb(223, 223, 223)" d="M0.185,14.815 C-0.020,14.608 -0.020,14.273 0.185,14.067 L6.742,7.491 L0.185,0.902 C-0.020,0.696 -0.020,0.361 0.185,0.154 C0.390,-0.052 0.724,-0.052 0.929,0.154 L7.846,7.104 C7.948,7.207 8.000,7.336 8.000,7.478 C8.000,7.607 7.948,7.749 7.846,7.852 L0.929,14.802 C0.724,15.021 0.390,15.021 0.185,14.815 Z"
              />
            </svg>
          </a>
            <?php } ?>
        </div>
        <h3 class="header"> <?php echo $heading_title; ?></h3>
        <p class="article"><?php echo $text_model; ?> <?php echo $model; ?></p>
        <p class="counter"><strong><?php foreach ($res1->rows as $row) { echo $row['hosts']; } ?></strong> <?php echo $text_bride; ?> <strong>8</strong> <?php echo $text_hours; ?></p>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b87f547f481349e"></script>
        <div class="addthis_toolbox">
        <div class="line social custom_images">
          <a class="like" data-id="<?php echo $product_id; ?>">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="20px">
              <path fill-rule="evenodd"  fill="rgb(197, 166, 114)" d="M21.967,5.962 C21.646,2.505 19.143,-0.003 16.011,-0.003 C13.924,-0.003 12.014,1.095 10.939,2.854 C9.873,1.072 8.041,-0.003 5.988,-0.003 C2.856,-0.003 0.353,2.505 0.032,5.962 C0.007,6.114 -0.097,6.918 0.219,8.229 C0.675,10.119 1.729,11.838 3.266,13.200 L10.933,20.002 L18.732,13.200 C20.269,11.838 21.323,10.119 21.779,8.229 C22.096,6.918 21.992,6.115 21.967,5.962 Z" / >
              </svg>
          </a>
          <a class="addthis_button_more">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="20px">
              <path fill-rule="evenodd" fill="rgb(187, 187, 187)" d="M14.727,13.220 C13.853,13.220 13.031,13.573 12.413,14.213 C12.241,14.391 12.094,14.586 11.966,14.792 L6.311,11.237 C6.461,10.849 6.545,10.427 6.545,9.985 C6.545,9.543 6.461,9.121 6.311,8.733 L11.968,5.204 C12.549,6.150 13.567,6.780 14.727,6.780 C16.532,6.780 18.000,5.259 18.000,3.390 C18.000,1.520 16.532,-0.000 14.727,-0.000 C12.923,-0.000 11.454,1.520 11.454,3.390 C11.454,3.815 11.534,4.221 11.673,4.597 L6.009,8.130 C5.424,7.207 4.416,6.595 3.273,6.595 C1.468,6.595 -0.000,8.116 -0.000,9.985 C-0.000,11.854 1.468,13.375 3.273,13.375 C4.416,13.375 5.423,12.763 6.009,11.839 L11.672,15.400 C11.531,15.781 11.454,16.189 11.454,16.610 C11.454,17.515 11.795,18.367 12.413,19.007 C13.031,19.647 13.853,20.000 14.727,20.000 C15.601,20.000 16.423,19.647 17.041,19.007 C17.659,18.367 18.000,17.515 18.000,16.610 C18.000,15.704 17.659,14.853 17.041,14.213 C16.423,13.573 15.601,13.220 14.727,13.220 Z"
              />
            </svg>
          </a>
          <a class="addthis_button_pinterest">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17px" height="20px">
              <path fill-rule="evenodd" fill="rgb(187, 187, 187)" d="M8.784,-0.000 C3.050,-0.000 -0.000,3.513 -0.000,7.343 C-0.000,9.119 1.038,11.334 2.700,12.037 C2.953,12.146 3.090,12.099 3.146,11.876 C3.196,11.706 3.415,10.887 3.521,10.501 C3.553,10.377 3.536,10.269 3.432,10.153 C2.880,9.543 2.442,8.432 2.442,7.389 C2.442,4.717 4.663,2.124 8.444,2.124 C11.714,2.124 14.001,4.154 14.001,7.058 C14.001,10.339 12.185,12.609 9.824,12.609 C8.518,12.609 7.545,11.629 7.853,10.417 C8.226,8.973 8.957,7.420 8.957,6.379 C8.957,5.445 8.405,4.673 7.278,4.673 C5.948,4.673 4.869,5.931 4.869,7.622 C4.869,8.695 5.266,9.421 5.266,9.421 C5.266,9.421 3.951,14.501 3.706,15.450 C3.293,17.056 3.762,19.658 3.803,19.881 C3.828,20.005 3.973,20.044 4.054,19.943 C4.183,19.780 5.774,17.611 6.220,16.043 C6.382,15.472 7.048,13.156 7.048,13.156 C7.486,13.912 8.750,14.546 10.097,14.546 C14.104,14.546 17.000,11.179 17.000,7.002 C16.986,2.996 13.400,-0.000 8.784,-0.000 Z"
              />
            </svg>
          </a>
          <a class="addthis_button_whatsapp">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="21px">
              <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M10.675,-0.000 C4.982,-0.000 0.350,4.632 0.350,10.325 C0.350,12.126 0.821,13.896 1.714,15.452 L0.018,20.539 C-0.022,20.661 0.007,20.795 0.095,20.889 C0.162,20.961 0.255,21.000 0.350,21.000 C0.380,21.000 0.410,20.996 0.439,20.988 L5.993,19.527 C7.435,20.262 9.051,20.650 10.675,20.650 C16.369,20.650 21.000,16.018 21.000,10.325 C21.000,4.632 16.369,-0.000 10.675,-0.000 ZM17.118,15.013 L16.503,15.629 C15.726,16.406 14.859,16.800 13.927,16.800 L13.927,16.800 C12.670,16.800 11.315,16.082 9.901,14.667 L6.333,11.100 C5.217,9.984 4.532,8.900 4.295,7.878 C4.008,6.636 4.370,5.499 5.372,4.498 L5.987,3.882 C6.383,3.486 7.023,3.389 7.519,3.652 C8.451,4.145 9.416,5.760 9.524,5.943 C9.717,6.281 9.776,6.631 9.693,6.931 C9.629,7.161 9.485,7.347 9.277,7.475 C8.994,7.711 8.662,8.006 8.604,8.081 C8.248,8.605 8.289,9.008 8.747,9.467 L11.534,12.253 C11.996,12.716 12.392,12.754 12.925,12.393 C12.995,12.338 13.289,12.006 13.526,11.723 C13.703,11.431 13.992,11.271 14.344,11.271 C14.575,11.271 14.820,11.341 15.054,11.474 C15.240,11.584 16.856,12.549 17.349,13.480 C17.618,13.990 17.526,14.605 17.118,15.013 Z"
              />
            </svg>
          </a>
        </div></div><a class="btn_simple" href="#contat"><?php echo $text_getprice; ?></a>
        <div class="desc">
          <p><?php echo $text_garanted1; ?></p>
          <p><?php echo $text_garanted2; ?></p>
        </div>
        <div class="desc">
          <p><?php echo $text_garanted3; ?></p>
          <p><?php echo $text_garanted4; ?></p>
        </div>
        <div class="desc_tabs">
          <ul class="tabs_caption">
            <li><?php echo $tab_description; ?>
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="5px">
                <path fill-rule="evenodd" fill="rgb(104, 104, 104)" d="M9.892,0.096 C9.754,-0.032 9.530,-0.032 9.392,0.096 L4.994,4.211 L0.588,0.096 C0.450,-0.032 0.226,-0.032 0.088,0.096 C-0.050,0.225 -0.050,0.435 0.088,0.563 L4.735,4.903 C4.804,4.968 4.891,5.000 4.985,5.000 C5.072,5.000 5.167,4.968 5.236,4.903 L9.883,0.563 C10.030,0.435 10.030,0.225 9.892,0.096 Z"
                />
              </svg>
            </li>
            <li class="active"><?php echo $text_specification; ?>
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="5px">
                <path fill-rule="evenodd" fill="rgb(104, 104, 104)" d="M9.892,0.096 C9.754,-0.032 9.530,-0.032 9.392,0.096 L4.994,4.211 L0.588,0.096 C0.450,-0.032 0.226,-0.032 0.088,0.096 C-0.050,0.225 -0.050,0.435 0.088,0.563 L4.735,4.903 C4.804,4.968 4.891,5.000 4.985,5.000 C5.072,5.000 5.167,4.968 5.236,4.903 L9.883,0.563 C10.030,0.435 10.030,0.225 9.892,0.096 Z"
                />
              </svg>
            </li>
          </ul>
          <div class="tabs_item">
            <p><?php echo $description; ?></p>
          </div>
          <div class="tabs_item active">
            <p><?php echo $specification; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php if ($imagesbride) { ?>
<section class="bride_block">
  <div class="container">
    <h3 class="header"><?php echo $text_brides; ?></h3>
    <p class="sub_header"><?php echo $text_brides1; ?></p>
    <?php if ($imagesbride) { ?>
    <div class="display_slider">
      <?php if ($imagesbride) { ?>
      <?php foreach ($imagesbride as $image) { ?>
      <div class="img" style="background-image:url('<?php echo $image['thumb']; ?>')"></div>
      <?php } ?>
      <?php } ?>
    </div>
    <div class="nav_slider">
      <?php if ($imagesbride) { ?>
      <?php foreach ($imagesbride as $image) { ?>
      <div class="wrapper">
        <div class="img" style="background-image:url('<?php echo $image['thumb']; ?>')"></div>
      </div>
      <?php } ?>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
</section>
<?php } ?>

<?php if ($products) { ?>
<section class="trend_block">
  <div class="container">
    <h3 class="header"><?php echo $text_discover1; ?></h3>
    <h3 class="sub_header"><?php echo $text_discover2; ?></h3>
    <div class="trend_list">
      <?php foreach ($products as $product) { ?>
      <div class="wrapper">
        <a class="trend__item" href="<?php echo $product['href']; ?>" style="background-image:url('<?php echo $product['thumb']; ?>')">
          <div class="_hover">
            <p class="had"><?php echo $product['name']; ?></p>
            <p class="text"><?php echo $product['description']; ?></p>
          </div>
        </a>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<?php } ?>

<!-- contact form start -->
<section class="contact_block" id="contat">
  <div class="__left"></div>
  <div class="__right">
    <div class="entity">
      <h1 class="header"><?php echo $text_contact1; ?></h1>
      <p class="sub_header"><?php echo $text_contact2; ?></p>
      <form id="contact">
        <div class="line">
          <input type="text" placeholder="<?php echo $text_contact3; ?>" name="name">
          <input type="text" placeholder="<?php echo $text_contact4; ?>*" name="email">
        </div>
        <textarea placeholder="<?php echo $text_contact5; ?>" name="message"></textarea>
        <div class="btn_simple"><?php echo $text_contact6; ?></div>
      </form>
    </div>
  </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
  $(document).ready(function() {
    $(".like").bind("click", function(e) {
      e.preventDefault();
      var link = $(this);
      var id = link.data('id');
      $.ajax({
        url: "index.php?route=product/category/like",
        type: "POST",
        data: {id: id},
        success: function(data) { $('.social .like').addClass('active'); }
      });
    });
    var link = $('.like');
    var id = link.data('id');
    if ($.cookie('like'+id)) { $('.social .like').addClass('active'); }
  });
</script>
<!-- contact form end -->

  <script type="text/javascript">
 $(document).ready(function(){
    $(".btn_simple").click(function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});
</script>


<!--
<div class="container hidden">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <?php if ($thumb || $images) { ?>
          <ul class="thumbnails">
            <?php if ($thumb) { ?>
            <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
          </div>
        </div>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="btn-group">
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>
          </div>
          <h1><?php echo $heading_title; ?></h1>
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
          </ul>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special) { ?>
            <li>
              <h2><?php echo $price; ?></h2>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;"><?php echo $price; ?></span></li>
            <li>
              <h2><?php echo $special; ?></h2>
            </li>
            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <div id="product">
            <?php if ($options) { ?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>                    
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                    <?php } ?>
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring; ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
            <hr>

          </div>
          <?php } ?>
        </div>
      </div>

      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>-->

<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
    grecaptcha.reset();
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});

$(document).ready(function() {
	var hash = window.location.hash;
	if (hash) {
		var hashpart = hash.split('#');
		var  vals = hashpart[1].split('-');
		for (i=0; i<vals.length; i++) {
			$('#product').find('select option[value="'+vals[i]+'"]').attr('selected', true).trigger('select');
			$('#product').find('input[type="radio"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
			$('#product').find('input[type="checkbox"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
		}
	}
})
//--></script>
<script>
  $(document).ready(function() {

    $('.product_block .display_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: false,
      infinite: true,
      asNavFor: '.product_block .nav_slider',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            dots: true,
            arrows: true,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
          }
        },
      ]
    });
    $('.product_block .nav_slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.product_block .display_slider',
      dots: false,
      vertical: true,
      centerMode: false,
      focusOnSelect: true,
      arrows:true,
      infinite: true,
      prevArrow: '<div class="prev"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="10px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.185,9.807 C0.461,10.064 0.909,10.064 1.185,9.807 L9.980,1.578 L18.793,9.807 C19.069,10.064 19.517,10.064 19.793,9.807 C20.069,9.549 20.069,9.131 19.793,8.873 L10.498,0.193 C10.360,0.064 10.187,-0.000 9.997,-0.000 C9.825,-0.000 9.635,0.064 9.497,0.193 L0.202,8.873 C-0.091,9.131 -0.091,9.549 0.185,9.807 Z"/></svg></div>',
      nextArrow: '<div class="next"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="10px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M19.815,0.193 C19.539,-0.065 19.091,-0.065 18.815,0.193 L10.019,8.422 L1.207,0.193 C0.931,-0.065 0.483,-0.065 0.206,0.193 C-0.069,0.450 -0.069,0.869 0.206,1.127 L9.502,9.807 C9.640,9.935 9.813,10.000 10.002,10.000 C10.175,10.000 10.364,9.935 10.502,9.807 L19.798,1.127 C20.091,0.869 20.091,0.450 19.815,0.193 Z"/></svg></div>',
    });
    $('.bride_block .display_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<div class="prev"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.207 C10.064,0.931 10.064,0.483 9.807,0.207 C9.549,-0.069 9.130,-0.069 8.873,0.207 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.175 0.064,10.364 0.193,10.503 L8.873,19.798 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg></div>',
      nextArrow: '<div class="next"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.176 C-0.064,0.900 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.869,-0.101 1.127,0.175 L9.807,9.471 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.767 C0.869,20.060 0.451,20.060 0.193,19.784 Z"/></svg></div>',
      fade: false,
      infinite: true,
      asNavFor: '.bride_block .nav_slider',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            dots: true
          }
        },
      ]
    });
    $('.bride_block .nav_slider').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      asNavFor: '.bride_block .display_slider',
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      infinite: true,

    });
    $(".trend_block .trend_list").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            arrows: true,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            autoplay: true,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows: true,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            autoplay: true
          }
        },
        {
          breakpoint: 374,
          settings: {
            slidesToShow: 1,
            prevArrow: '<svg class="prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="21px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M9.807,19.815 C10.064,19.539 10.064,19.091 9.807,18.815 L1.578,10.019 L9.807,1.206 C10.064,0.931 10.064,0.482 9.807,0.206 C9.549,-0.070 9.130,-0.070 8.873,0.206 L0.193,9.502 C0.064,9.640 -0.000,9.812 -0.000,10.002 C-0.000,10.174 0.064,10.364 0.193,10.502 L8.873,19.797 C9.130,20.091 9.549,20.091 9.807,19.815 Z"/></svg>',
            nextArrow: '<svg class="next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="20px"><path fill-rule="evenodd"  fill="rgb(187, 187, 187)" d="M0.193,19.784 C-0.064,19.508 -0.064,19.060 0.193,18.784 L8.422,9.988 L0.193,1.175 C-0.064,0.899 -0.064,0.451 0.193,0.175 C0.451,-0.101 0.870,-0.101 1.127,0.175 L9.807,9.470 C9.936,9.609 10.000,9.781 10.000,9.971 C10.000,10.143 9.936,10.333 9.807,10.471 L1.127,19.766 C0.870,20.060 0.451,20.060 0.193,19.784 Z"/></svg>',
            dots: true,
            arrows: true,
          }
        }
      ]
    });
  });
</script>
<?php echo $footer; ?>
