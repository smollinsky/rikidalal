<?php echo $header; ?>
<div class="container">
  <div class="bread_crumbs_block">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>
<?php if ($articles) { ?>
<section class="blog_block">
  <div class="container">
    <h1 class="header"><?php echo $heading_title; ?></h1>
    <div class="blog_list">
      <?php foreach ($articles as $article) { ?>
      <div class="__item">
        <div class="img" style="background-image:url('<?php echo $article['thumb']; ?>')"></div>
        <div class="cont">
          <p class="had"><?php echo $article['name']; ?></p>
          <p class="text"><?php echo $article['description']; ?></p><a class="btn_simple" href="<?php echo $article['href']; ?>"><?php echo $button_more; ?></a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<section class="pagination_block"><?php echo $pagination; ?></section>
<?php } else { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>

<?php echo $footer; ?>