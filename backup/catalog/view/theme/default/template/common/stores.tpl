<?php echo $header; ?>
<div class="container">
    <div class="bread_crumbs_block">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
</div>

<section class="search_block">
    <div class="container">
        <form method="" name="Form">
            <input placeholder="<?php echo $text_botique_search1; ?>" name="search"  value="" class="who"  autocomplete="off">
            <button class="btn_simple btn_submit" type="submit"><?php echo $text_botique_search2; ?></button>
            <ul class="search_result"></ul>
        </form>
        <div id="list" class="main__list"></div>
        <p class="header"><?php echo $text_botique; ?></p>
        <p class="sub_header"><?php echo $text_botique1; ?></p>
        <?php if ($locations) { ?>
        <div class="important__list">
            <?php foreach ($locations as $location) { ?>
            <?php if ($location['status'] == 1) { ?>
            <div class="__item" style="background-image:url('image/<?php echo $location['image']; ?>')">
                <div class="entity">
                    <p class="had"><?php echo $location['city']; ?>, <?php foreach ($countries as $country) { ?>
                        <?php if ($country['country_id'] == $location['country']) { ?><?php echo $country['name']; ?><?php } ?><?php } ?></p>
                    <p class="sub_had"><?php echo $location['address']; ?></p><a class="tel" href="tel:<?php echo $location['telephone']; ?>"><?php echo $location['telephone']; ?></a>
                    <div class="_link">
                        <?php if ($location['telephone']) { ?>
                        <a href="tel:<?php echo $location['telephone']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M18.464,9.647 C18.119,7.628 17.166,5.792 15.713,4.340 C14.182,2.813 12.242,1.843 10.103,1.543 L10.320,-0.001 C12.796,0.345 15.039,1.465 16.812,3.237 C18.493,4.923 19.596,7.045 20.000,9.384 L18.464,9.647 ZM14.028,5.947 C15.039,6.962 15.705,8.240 15.946,9.651 L14.411,9.912 C14.223,8.822 13.711,7.832 12.929,7.049 C12.101,6.221 11.052,5.701 9.899,5.539 L10.116,3.995 C11.605,4.202 12.958,4.877 14.028,5.947 ZM6.528,10.907 C7.594,12.214 8.796,13.375 10.232,14.278 C10.540,14.469 10.881,14.611 11.202,14.786 C11.364,14.878 11.477,14.848 11.610,14.711 C12.097,14.211 12.592,13.720 13.087,13.230 C13.736,12.584 14.552,12.584 15.205,13.230 C16.000,14.020 16.795,14.811 17.586,15.606 C18.248,16.272 18.244,17.088 17.578,17.762 C17.128,18.216 16.650,18.649 16.225,19.123 C15.605,19.818 14.831,20.042 13.940,19.993 C12.646,19.922 11.456,19.493 10.307,18.935 C7.756,17.695 5.575,15.977 3.748,13.804 C2.396,12.198 1.280,10.445 0.548,8.473 C0.190,7.520 -0.064,6.542 0.015,5.505 C0.065,4.868 0.302,4.324 0.772,3.878 C1.280,3.396 1.759,2.892 2.258,2.401 C2.907,1.760 3.723,1.760 4.376,2.397 C4.780,2.792 5.176,3.196 5.575,3.595 C5.962,3.986 6.349,4.369 6.736,4.760 C7.419,5.447 7.419,6.246 6.740,6.928 C6.253,7.420 5.771,7.911 5.275,8.389 C5.146,8.519 5.134,8.623 5.201,8.781 C5.529,9.563 6.000,10.258 6.528,10.907 Z"
                                />
                            </svg>
                        </a>
                        <?php } ?>
                        <?php if ($location['email']) { ?>
                        <a href="mailto:<?php echo $location['email']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="16px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M13.750,8.126 L21.000,1.399 L21.000,14.696 L13.750,8.126 ZM0.477,0.436 C0.747,0.166 1.110,-0.001 1.510,-0.001 L19.490,-0.001 C19.892,-0.001 20.256,0.164 20.525,0.433 L10.500,9.599 L0.477,0.436 ZM-0.000,14.696 L-0.000,1.404 L7.249,8.126 L-0.000,14.696 ZM10.500,11.200 L13.098,8.774 L20.522,15.565 C20.253,15.834 19.890,16.000 19.490,16.000 L1.510,16.000 C1.108,16.000 0.744,15.834 0.474,15.565 L7.902,8.774 L10.500,11.200 Z"
                                />
                            </svg>
                        </a>
                        <?php } ?>
                        <?php if ($location['whatsapp']) { ?>
                        <a href="whatsapp://send?text=<?php echo $location['whatsapp']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="21px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M10.675,-0.001 C4.982,-0.001 0.350,4.631 0.350,10.324 C0.350,12.125 0.821,13.895 1.714,15.452 L0.018,20.539 C-0.022,20.661 0.007,20.795 0.095,20.888 C0.162,20.961 0.255,20.999 0.350,20.999 C0.380,20.999 0.410,20.996 0.439,20.988 L5.993,19.527 C7.435,20.261 9.051,20.649 10.675,20.649 C16.369,20.649 21.000,16.018 21.000,10.324 C21.000,4.631 16.369,-0.001 10.675,-0.001 ZM17.118,15.013 L16.503,15.628 C15.726,16.405 14.859,16.799 13.927,16.799 L13.927,16.799 C12.670,16.799 11.315,16.082 9.901,14.667 L6.333,11.099 C5.217,9.983 4.531,8.900 4.295,7.877 C4.008,6.636 4.370,5.498 5.372,4.497 L5.987,3.882 C6.383,3.486 7.023,3.389 7.519,3.651 C8.451,4.144 9.416,5.759 9.524,5.943 C9.717,6.281 9.776,6.631 9.693,6.931 C9.629,7.160 9.485,7.347 9.277,7.474 C8.994,7.710 8.662,8.005 8.604,8.080 C8.248,8.605 8.289,9.007 8.747,9.466 L11.534,12.253 C11.996,12.715 12.392,12.753 12.925,12.392 C12.995,12.337 13.289,12.006 13.526,11.723 C13.703,11.431 13.992,11.271 14.344,11.271 C14.575,11.271 14.820,11.340 15.054,11.474 C15.240,11.583 16.856,12.548 17.349,13.480 C17.618,13.989 17.526,14.605 17.118,15.013 Z"
                                />
                            </svg>
                        </a>
                        <?php } ?>
                    </div><a class="btn_simple" href="#gov"><?php echo $text_detail; ?></a>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
        <p class="header"><?php echo $text_botique2; ?></p>
        <p class="sub_header"><?php echo $text_botique3; ?></p>
        <div class="fillter">
            <select name="europe" id="info_search" class="info_search1" data-placeholder="Europe">
            <option></option>
                <?php foreach ($unique_location as $location) { ?>
                <?php if ($location['continent_id'] == 1) { ?>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $location['country']) { ?>
                <option value="<?php echo $country['country_id']; ?>">
                    <?php echo $country['name']; ?>
                </option><?php } ?>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </select>
            <select name="america" id="info_search" class="info_search2" data-placeholder="North America">
            <option value="0"></option>
                <?php foreach ($unique_location as $location) { ?>
                <?php if ($location['continent_id'] == 2) { ?>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $location['country']) { ?>
                <option value="<?php echo $country['country_id']; ?>">
                    <?php echo $country['name']; ?>
                </option><?php } ?>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </select>
            <select name="asia" id="info_search" class="info_search3" data-placeholder="Asia">
            <option></option>
                <?php foreach ($unique_location as $location) { ?>
                <?php if ($location['continent_id'] == 3) { ?>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $location['country']) { ?>
                <option value="<?php echo $country['country_id']; ?>">
                    <?php echo $country['name']; ?>
                </option><?php } ?>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </select>
            <select name="australia" id="info_search" class="info_search4" data-placeholder="Australia">
            <option></option>
                <?php foreach ($unique_location as $location) { ?>
                <?php if ($location['continent_id'] == 4) { ?>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $location['country']) { ?>
                <option value="<?php echo $country['country_id']; ?>">
                    <?php echo $country['name']; ?>
                </option><?php } ?>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </select>
            <select name="more" id="info_search" class="info_search5" data-placeholder="More">
            <option></option>
                <?php foreach ($unique_location as $location) { ?>
                <?php if ($location['continent_id'] == 5) { ?>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $location['country']) { ?>
                <option value="<?php echo $country['country_id']; ?>">
                    <?php echo $country['name']; ?>
                </option><?php } ?>
                <?php } ?>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
        <?php if ($locations) { ?>
        <div class="main__list" id="txtHint">
            <?php foreach ($locations as $location) { ?>
            <div class="wrapper">
                <div class="__item">
                    <p class="had"><?php echo $location['city']; ?>, <?php foreach ($countries as $country) { ?><?php if ($country['country_id'] == $location['country']) { ?><?php echo $country['name']; ?><?php } ?><?php } ?></p>
                    <p class="sub_had"><?php echo $location['address']; ?></p><a class="tel" href="tel:<?php echo $location['telephone']; ?>"><?php echo $location['telephone']; ?></a>
                    <div class="_link">
                        <?php if ($location['telephone']) { ?>
                        <a href="tel:<?php echo $location['telephone']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M18.464,9.647 C18.119,7.628 17.166,5.792 15.713,4.340 C14.182,2.813 12.242,1.843 10.103,1.543 L10.320,-0.001 C12.796,0.345 15.039,1.465 16.812,3.237 C18.493,4.923 19.596,7.045 20.000,9.384 L18.464,9.647 ZM14.028,5.947 C15.039,6.962 15.705,8.240 15.946,9.651 L14.411,9.912 C14.223,8.822 13.711,7.832 12.929,7.049 C12.101,6.221 11.052,5.701 9.899,5.539 L10.116,3.995 C11.605,4.202 12.958,4.877 14.028,5.947 ZM6.528,10.907 C7.594,12.214 8.796,13.375 10.232,14.278 C10.540,14.469 10.881,14.611 11.202,14.786 C11.364,14.878 11.477,14.848 11.610,14.711 C12.097,14.211 12.592,13.720 13.087,13.230 C13.736,12.584 14.552,12.584 15.205,13.230 C16.000,14.020 16.795,14.811 17.586,15.606 C18.248,16.272 18.244,17.088 17.578,17.762 C17.128,18.216 16.650,18.649 16.225,19.123 C15.605,19.818 14.831,20.042 13.940,19.993 C12.646,19.922 11.456,19.493 10.307,18.935 C7.756,17.695 5.575,15.977 3.748,13.804 C2.396,12.198 1.280,10.445 0.548,8.473 C0.190,7.520 -0.064,6.542 0.015,5.505 C0.065,4.868 0.302,4.324 0.772,3.878 C1.280,3.396 1.759,2.892 2.258,2.401 C2.907,1.760 3.723,1.760 4.376,2.397 C4.780,2.792 5.176,3.196 5.575,3.595 C5.962,3.986 6.349,4.369 6.736,4.760 C7.419,5.447 7.419,6.246 6.740,6.928 C6.253,7.420 5.771,7.911 5.275,8.389 C5.146,8.519 5.134,8.623 5.201,8.781 C5.529,9.563 6.000,10.258 6.528,10.907 Z"
                                />
                            </svg>
                        </a>
                        <?php } ?>
                        <?php if ($location['email']) { ?>
                        <a href="mailto:<?php echo $location['email']; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="16px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M13.750,8.126 L21.000,1.399 L21.000,14.696 L13.750,8.126 ZM0.477,0.436 C0.747,0.166 1.110,-0.001 1.510,-0.001 L19.490,-0.001 C19.892,-0.001 20.256,0.164 20.525,0.433 L10.500,9.599 L0.477,0.436 ZM-0.000,14.696 L-0.000,1.404 L7.249,8.126 L-0.000,14.696 ZM10.500,11.200 L13.098,8.774 L20.522,15.565 C20.253,15.834 19.890,16.000 19.490,16.000 L1.510,16.000 C1.108,16.000 0.744,15.834 0.474,15.565 L7.902,8.774 L10.500,11.200 Z"
                                />
                            </svg>
                        </a>
                        <?php } ?>
                    </div><a class="btn_simple" href="#gov" data-store="{"name":"<?php echo $location['address']; ?>", "address":"<?php echo $location['address1']; ?>", "phone":"<?php echo $location['telephone']; ?>", "email":"<?php echo $location['email']; ?>"}"><?php echo $text_detail; ?></a>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</section>
<section class="popap_type_one_block" id="gov">
            <div class="entity">
                <p class="header">Once Upon A Time</p>
                <div class="line top">
                    <div class="_left cont">
                        <div class="line">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="20px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M7.500,-0.000 C3.364,-0.000 -0.000,3.249 -0.000,7.243 C-0.000,12.199 6.712,19.476 6.997,19.783 C7.266,20.071 7.734,20.071 8.002,19.783 C8.288,19.476 15.000,12.199 15.000,7.243 C15.000,3.249 11.635,-0.000 7.500,-0.000 ZM7.500,10.887 C5.419,10.887 3.726,9.252 3.726,7.243 C3.726,5.233 5.419,3.598 7.500,3.598 C9.580,3.598 11.273,5.233 11.273,7.243 C11.273,9.252 9.580,10.887 7.500,10.887 Z"
                                />
                            </svg>
                            <p class="text">Leoforos Papanikoli 23, Larnaca</p>
                        </div>
                        <div class="line">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="20px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M18.464,9.647 C18.119,7.628 17.166,5.792 15.713,4.340 C14.182,2.813 12.242,1.843 10.103,1.543 L10.320,-0.001 C12.796,0.345 15.039,1.465 16.812,3.237 C18.493,4.923 19.596,7.045 20.000,9.384 L18.464,9.647 ZM14.028,5.947 C15.039,6.962 15.705,8.240 15.946,9.651 L14.411,9.912 C14.223,8.822 13.711,7.832 12.929,7.049 C12.101,6.221 11.052,5.701 9.899,5.539 L10.116,3.995 C11.605,4.202 12.958,4.877 14.028,5.947 ZM6.528,10.907 C7.594,12.214 8.796,13.375 10.232,14.278 C10.540,14.469 10.881,14.611 11.202,14.786 C11.364,14.878 11.477,14.848 11.610,14.711 C12.097,14.211 12.592,13.720 13.087,13.230 C13.736,12.584 14.552,12.584 15.205,13.230 C16.000,14.020 16.795,14.811 17.586,15.606 C18.248,16.272 18.244,17.088 17.578,17.762 C17.128,18.216 16.650,18.649 16.225,19.123 C15.605,19.818 14.831,20.042 13.940,19.993 C12.646,19.922 11.456,19.493 10.307,18.935 C7.756,17.695 5.575,15.977 3.748,13.804 C2.396,12.198 1.280,10.445 0.548,8.473 C0.190,7.520 -0.064,6.542 0.015,5.505 C0.065,4.868 0.302,4.324 0.772,3.878 C1.280,3.396 1.759,2.892 2.258,2.401 C2.907,1.760 3.723,1.760 4.376,2.397 C4.780,2.792 5.176,3.196 5.575,3.595 C5.962,3.986 6.349,4.369 6.736,4.760 C7.419,5.447 7.419,6.246 6.740,6.928 C6.253,7.420 5.771,7.911 5.275,8.389 C5.146,8.519 5.134,8.623 5.201,8.781 C5.529,9.563 6.000,10.258 6.528,10.907 Z"
                                />
                            </svg><a class="text tel" href="#">+357 2466 0530</a>
                        </div>
                        <div class="line">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="16px">
                                <path fill-rule="evenodd" fill="rgb(200, 200, 200)" d="M13.750,8.126 L21.000,1.399 L21.000,14.696 L13.750,8.126 ZM0.477,0.436 C0.747,0.166 1.110,-0.001 1.510,-0.001 L19.490,-0.001 C19.892,-0.001 20.256,0.164 20.525,0.433 L10.500,9.599 L0.477,0.436 ZM-0.000,14.696 L-0.000,1.404 L7.249,8.126 L-0.000,14.696 ZM10.500,11.200 L13.098,8.774 L20.522,15.565 C20.253,15.834 19.890,16.000 19.490,16.000 L1.510,16.000 C1.108,16.000 0.744,15.834 0.474,15.565 L7.902,8.774 L10.500,11.200 Z"
                                />
                            </svg><a class="text" href="#">info@onceuponatimedream.com</a>
                        </div>
                    </div>
                    <div class="_right">
                        <div class="line img">
                            <img src="catalog/view/theme/default/img/minified-svg/logo_dalal_mobile.svg" alt="logo">
                            <img src="catalog/view/theme/default/img/content/noya.png" alt="logo">
                        </div>
                        <p class="text">Riki Dalal and NOYA Collections</p><a class="text tel" href="#">Available</a>
                    </div>
                </div>
                <p class="sub_header">Fill out the form for details, appointments and prices:</p>
                <div class="line top">
                    <div class="_left">
                        <div class="input">
                            <p class="text">Appointment Preferred Date</p>
                            <input placeholder="Enter date">
                        </div>
                    </div>
                    <div class="_right">
                        <div class="input">
                            <p class="text">Preferred Appointment Times<sup>*</sup>
                            </p>
                            <label for="1">
                                <input type="checkbox" id="1">
                                <p class="text">Morning</p>
                            </label>
                            <label for="2">
                                <input type="checkbox" id="2">
                                <p class="text">Afternoon</p>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="line">
                    <div class="_left">
                        <div class="input">
                            <p class="text">Country<sup>*</sup>
                            </p>
                            <select>
                                <option>United Kingdom</option>
                                <option>United Kingdom</option>
                                <option>United Kingdom</option>
                            </select>
                        </div>
                    </div>
                    <div class="_right">
                        <div class="input">
                            <p class="text">City<sup>*</sup>
                            </p>
                            <input placeholder="Enter city">
                        </div>
                    </div>
                </div>
            </div>
            <p class="close">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="13px">
                    <path fill-rule="evenodd" fill="rgb(187, 187, 187)" d="M11.753,12.871 L6.495,7.614 L1.237,12.871 C1.082,13.028 0.830,13.028 0.675,12.871 L0.113,12.311 C-0.043,12.156 -0.043,11.904 0.113,11.749 L5.371,6.491 L0.113,1.233 C-0.043,1.078 -0.043,0.827 0.113,0.671 L0.675,0.109 C0.830,-0.048 1.082,-0.048 1.237,0.109 L6.495,5.367 L11.753,0.109 C11.908,-0.048 12.160,-0.048 12.315,0.109 L12.877,0.671 C13.032,0.827 13.032,1.078 12.877,1.233 L7.619,6.491 L12.877,11.749 C13.032,11.904 13.032,12.156 12.877,12.311 L12.315,12.871 C12.160,13.028 11.908,13.028 11.753,12.871 Z"
                    />
                </svg>
            </p><a class="btn_simple" href="#">Book a meeting</a>
        </section>
<script type="text/javascript">
    $('.fillter select').on('change', function() {
        $.ajax({
            url: 'index.php?route=common/stores/getBoutiques&id=' + this.value,
            dataType: 'html',
            success: function(htmlText) {
                $('#txtHint').html(htmlText);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>

<script>
    $(function(){
        $('.who').bind("change keyup input click", function() {
            if(this.value.length >= 1){
                $.ajax({
                    type: 'post',
                    url: "index.php?route=common/stores/autocomplete",
                    data: {'search':this.value},
                    response: 'text',
                    success: function(data){
                        $(".search_result").html(data).fadeIn();
                    }
                })
            }
        })

        $(".search_result").hover(function(){
            $(".who").blur();
        })

        $(".search_result").on("click", "li", function(){
            s_user = $(this).find('.value').text();
            $(".who").val(s_user);
            $(".search_result").fadeOut();
        })

        $('form[name=Form]').submit(function(event){
            event.preventDefault();
            city = $(".who").val();
            $.ajax({
                url: 'index.php?route=common/stores/show_autocomplete_result&city=' + city,
                dataType: 'html',
                success: function(htmlText) {
                    $('#list').html(htmlText);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
</script>
        <script type="text/javascript">
        $('.info_search1').styler({
              onSelectOpened: function() {
                $('.info_search2 .jq-selectbox__select .jq-selectbox__select-text').text('North America');
                $('.info_search3 .jq-selectbox__select .jq-selectbox__select-text').text('Asia');
                $('.info_search4 .jq-selectbox__select .jq-selectbox__select-text').text('Australia');
                $('.info_search5 .jq-selectbox__select .jq-selectbox__select-text').text('More');
              }
            });
        $('.info_search2').styler({
              onSelectOpened: function() {
                $('.info_search1 .jq-selectbox__select .jq-selectbox__select-text').text('Europe');
                $('.info_search3 .jq-selectbox__select .jq-selectbox__select-text').text('Asia');
                $('.info_search4 .jq-selectbox__select .jq-selectbox__select-text').text('Australia');
                $('.info_search5 .jq-selectbox__select .jq-selectbox__select-text').text('More');
              }
            });
        $('.info_search3').styler({
              onSelectOpened: function() {
                $('.info_search1 .jq-selectbox__select .jq-selectbox__select-text').text('Europe');
                $('.info_search2 .jq-selectbox__select .jq-selectbox__select-text').text('North America');
                $('.info_search4 .jq-selectbox__select .jq-selectbox__select-text').text('Australia');
                $('.info_search5 .jq-selectbox__select .jq-selectbox__select-text').text('More');
              }
            });
         $('.info_search4').styler({
              onSelectOpened: function() {
                $('.info_search1 .jq-selectbox__select .jq-selectbox__select-text').text('Europe');
                $('.info_search2 .jq-selectbox__select .jq-selectbox__select-text').text('North America');
                $('.info_search3 .jq-selectbox__select .jq-selectbox__select-text').text('Asia');
                $('.info_search5 .jq-selectbox__select .jq-selectbox__select-text').text('More');
              }
            });
         $('.info_search5').styler({
              onSelectOpened: function() {
                $('.info_search1 .jq-selectbox__select .jq-selectbox__select-text').text('Europe');
                $('.info_search2 .jq-selectbox__select .jq-selectbox__select-text').text('North America');
                $('.info_search3 .jq-selectbox__select .jq-selectbox__select-text').text('Asia');
                $('.info_search4 .jq-selectbox__select .jq-selectbox__select-text').text('Australia');
              }
            });
        </script>

<?php echo $footer; ?>