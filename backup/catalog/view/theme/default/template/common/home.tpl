<?php echo $header; ?>
      <?php echo $content_top; ?>
      <section class="flagship_block">
        <div class="container">
          <div class="entity">
            <h1 class="header"><?php echo $text_search1; ?></h1>
            <p class="sub_header"><?php echo $text_search2; ?></p>
            <form action="/index.php?route=common/stores">
                <div class="hidden" id="address">
                    <input type="hidden" class="field" id="street_number" disabled="true"/>
                    <input type="hidden" class="field" id="route" disabled="true"/>
                    <input type="hidden" class="field" id="locality" disabled="true"/>
                    <input type="hidden" class="field" id="administrative_area_level_1" disabled="true"/>
                    <input type="hidden" class="field" id="postal_code" disabled="true"/>
                    <input type="hidden" class="field" id="country" disabled="true"/>
                </div>
                <script src="catalog/view/javascript/googleplaces.js"></script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATkzT6b0aZwVTBTTgqb0k7K_TKIAfHjs8&libraries=places&callback=initAutocomplete"
                        async defer></script>
              <input id="autocomplete" nFocus="geolocate()" type="text" placeholder="<?php echo $text_search3; ?>">
              <button class="btn_simple" type="submit"><?php echo $text_search4; ?></button>
            </form><a class="btn_effect" href="/index.php?route=common/stores"><span class="vert"></span><span class="horz"></span><span class="text"><?php echo $text_search5; ?></span></a>
            </div>
        </div>
      </section>
      <?php echo $column_right; ?>
      <!-- contact form start -->
        <section class="contact_block">
          <div class="__left"></div>
          <div class="__right">
            <div class="entity">
              <h1 class="header"><?php echo $text_contact1; ?></h1>
              <p class="sub_header"><?php echo $text_contact2; ?></p>
              <form id="contact">
                <div class="line">
                  <input type="text" placeholder="<?php echo $text_contact3; ?>" name="name">
                  <input type="text" placeholder="<?php echo $text_contact4; ?>*" name="email">
                </div>
                <textarea placeholder="<?php echo $text_contact5; ?>" name="message"></textarea>
                <div class="btn_simple"><?php echo $text_contact6; ?></div>
              </form>
            </div>
          </div>
        </section>
      <!-- contact form end -->
      <?php echo $content_bottom; ?>

<script type="text/javascript" src="catalog/view/javascript/contact.js"></script>

<?php echo $footer; ?>