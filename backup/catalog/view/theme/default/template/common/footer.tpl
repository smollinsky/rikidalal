<footer class="stick">
  <div class="container">
    <div class="footer">
      <div class="_part">
        <ul class="menu">
          <?php if ($categories) { ?>
          <li class="had"><?php echo $text_menu; ?></li>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          <?php } ?>
        </ul>
      </div>
      <div class="_part">
        <ul class="social_link">
          <li class="had"><?php echo $text_followus; ?></li>
          <li>
            <a href="<?php echo $social_facebook; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="10" height="16">
                <path fill-rule="evenodd" fill="#BBB" d="M8.699,0.002 L6.780,-0.001 C4.624,-0.001 4.000,0.757 4.000,2.999 L4.000,5.999 L1.000,5.999 C0.833,5.999 1.000,5.529 1.000,5.699 L1.000,8.164 C1.000,8.334 0.833,7.999 1.000,7.999 L4.000,7.999 L4.000,14.999 C4.000,15.168 3.833,14.999 4.000,14.999 L6.050,14.999 C6.216,14.999 6.000,15.168 6.000,14.999 L6.000,7.999 L9.000,7.999 C9.167,7.999 8.909,8.334 8.909,8.164 L9.000,5.999 C9.000,5.918 9.056,6.057 9.000,5.999 C8.943,5.942 9.080,5.999 9.000,5.999 L6.000,5.999 L6.000,2.999 C6.000,2.305 6.078,2.593 6.000,1.999 L9.000,1.999 C9.166,1.999 9.000,2.168 9.000,1.999 L9.000,0.308 C9.000,0.139 8.865,0.002 8.699,0.002 Z"
                />
              </svg>
              <p>Facebook</p>
            </a>
          </li>
          <li>
            <a href="<?php echo $social_pinterest; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
                />
              </svg>
              <p>Instagram</p>
            </a>
          </li>
          <li>
            <a href="<?php echo $social_instagram; ?>">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="15">
                <path fill-rule="evenodd" fill="#BBB" d="M4.522,9.920 C4.150,11.985 3.697,13.965 2.354,14.999 C1.940,11.878 2.963,9.534 3.438,7.045 C2.628,5.599 3.536,2.685 5.244,3.404 C7.347,4.286 3.423,8.783 6.057,9.345 C8.807,9.932 9.930,4.282 8.225,2.445 C5.761,-0.207 1.053,2.384 1.632,6.182 C1.773,7.111 2.677,7.393 1.993,8.674 C0.416,8.303 -0.055,6.984 0.006,5.225 C0.104,2.344 2.445,0.328 4.793,0.050 C7.762,-0.303 10.549,1.206 10.934,4.170 C11.367,7.516 9.594,11.139 6.418,10.878 C5.558,10.807 5.196,10.355 4.522,9.920 Z"
                />
              </svg>
              <p>Pinterest</p>
            </a>
          </li>
        </ul>
      </div>
      <div class="_part">
        <p class="had"><?php echo $text_newsletter; ?></p>
        <form id="newsletter">
          <div class="col-sm-4" id="error-msg"></div>
          <input id="input-newsletter" name="newsletter" placeholder="<?php echo $text_newsletter_text; ?>" value="">
          <div class="btn_simple" id="subcribe"><?php echo $text_subcribe; ?></div>
        </form>
      </div>
    </div>
    <p class="copyright"><?php echo $powered; ?></p>
  </div>
</footer>
<a class="top_button" href="javascript:void(0)">
            <img src="catalog/view/theme/default/img/content/top_but.png" alt="top">
        </a>
<script type="text/javascript" src="catalog/view/javascript/newsletter.js"></script>


<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>