<?php echo $header; ?>
<div class="container">
    <div class="bread_crumbs_block">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
</div>
<section class="events_block">
    <div class="container">
        <h1 class="header">In-store Events</h1>
        <div class="filter">
            <p class="had">Find your event near:</p>
            <form name="Search" class="line">
            <input placeholder="<?php echo $text_botique_search1; ?>" name="search"  value="" class="search"  autocomplete="off">
                 <ul class="search_result"></ul>
                <div class="sel">
                    <p>within</p>
                    <select>
                        <option>Any</option>
                        <option>Any</option>
                        <option>Any</option>
                    </select>
                    <p>km</p>
                </div>
                <button class="btn_simple">Search</button>
                <button class="clear">Clear</button>

            </form>
            <p class="had">When do you want to go?</p>
            <div class="line">
                <label class="active" for="any1">Any
                    <input type="checkbox" id="any1">
                </label>
                <label for="today2">Today
                    <input type="checkbox" value="<?php echo date("Y-m-d"); ?>" id="today2">
                </label>
                <label for="next3">Next 7 days
                    <input type="checkbox" id="next3">
                </label>
                <label for="next4">Next 30 days
                    <input type="checkbox" id="next4">
                </label>
                <div class="sel">
                    <p>From - to</p>
                    <input class="date" value="" id="date1" placeholder="24.05">
                    <p>-</p>
                    <input class="date" value="" id="date2" placeholder="24.06">
                    <p>km</p>
                </div>
                <button class="clear">Clear</button>
            </div>
        </div>
        <?php if ($events) { ?>
        <div class="events_list">
            <?php foreach ($events as $event) { ?>
            <div class="__item">
                <div class="line">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="21px">
                        <path fill-rule="evenodd" fill="rgb(197, 166, 114)" d="M19.000,21.000 C19.244,20.927 1.667,21.000 1.667,21.000 C0.746,21.000 -0.000,20.921 -0.000,20.000 L-0.000,4.541 C-0.000,3.620 0.079,3.000 1.000,3.000 L4.000,3.000 L4.000,1.000 C4.656,1.123 4.655,1.000 5.000,1.000 C5.345,1.000 5.687,0.967 6.000,1.000 L6.000,3.000 L9.000,3.000 L9.000,1.000 C9.750,1.061 9.655,1.000 10.000,1.000 C10.345,1.000 10.375,1.092 11.000,1.000 L11.000,3.000 L14.000,3.000 L14.000,1.000 C14.500,0.967 14.655,1.000 15.000,1.000 C15.345,1.000 15.281,1.311 16.000,1.000 L16.000,3.000 L19.000,3.000 C19.920,3.000 20.000,3.620 20.000,4.541 L20.000,19.333 C19.941,20.272 19.609,21.093 19.000,21.000 ZM19.000,4.000 L16.000,4.000 L16.000,6.000 C15.187,5.877 15.345,6.000 15.000,6.000 C14.655,6.000 14.594,6.095 14.000,6.000 L14.000,4.000 L11.000,4.000 L11.000,6.000 C10.281,5.908 10.345,6.000 10.000,6.000 C9.655,6.000 9.719,6.033 9.000,6.000 L9.000,4.000 L6.000,4.000 L6.000,6.000 C5.312,6.033 5.345,6.000 5.000,6.000 C4.655,6.000 5.125,5.970 4.000,6.000 L4.000,4.000 L1.000,4.000 L1.000,20.000 L19.000,20.000 L19.000,4.000 ZM7.000,10.000 L4.000,10.000 L4.000,9.000 L7.000,9.000 L7.000,10.000 ZM7.000,13.000 L4.000,13.000 L4.000,12.000 L7.000,12.000 L7.000,13.000 ZM7.000,16.000 L4.000,16.000 L4.000,15.000 L7.000,15.000 L7.000,16.000 ZM11.000,10.000 L9.000,10.000 L9.000,9.000 L11.000,9.000 L11.000,10.000 ZM11.000,13.000 L9.000,13.000 L9.000,12.000 L11.000,12.000 L11.000,13.000 ZM11.000,16.000 L9.000,16.000 L9.000,15.000 L11.000,15.000 L11.000,16.000 ZM16.000,10.000 L13.000,10.000 L13.000,9.000 L16.000,9.000 L16.000,10.000 ZM16.000,13.000 L13.000,13.000 L13.000,12.000 L16.000,12.000 L16.000,13.000 ZM16.000,16.000 L13.000,16.000 L13.000,15.000 L16.000,15.000 L16.000,16.000 Z"
                        />
                    </svg>
                    <p class="data"><?php echo $event['date_from']; ?> - <?php echo $event['date_to']; ?></p>
                </div>
                <p class="had"><?php echo $event['city']; ?>, <?php echo $event['country']; ?></p>
                <div class="place">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="20px">
                        <path fill-rule="evenodd" fill="rgb(197, 166, 114)" d="M7.500,-0.000 C3.364,-0.000 -0.000,3.249 -0.000,7.243 C-0.000,12.199 6.712,19.476 6.997,19.783 C7.266,20.072 7.734,20.071 8.002,19.783 C8.288,19.476 15.000,12.199 15.000,7.243 C15.000,3.249 11.635,-0.000 7.500,-0.000 ZM7.500,10.887 C5.419,10.887 3.726,9.252 3.726,7.243 C3.726,5.234 5.419,3.598 7.500,3.598 C9.581,3.598 11.273,5.234 11.273,7.243 C11.273,9.252 9.581,10.887 7.500,10.887 Z"
                        />
                    </svg>
                    <p><?php echo $event['store_name']; ?> (<?php echo $event['areakm']; ?> km)
                        <br/><?php echo $event['store_address']; ?></p>
                </div>
                <div class="img" style="background-image:url('<?php echo $event['thumb']; ?>')"></div><a class="btn_simple" href="#">Attend</a>
            </div>
        <?php } ?>
        </div>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <?php } ?>
    </div>
</section>
<script type="text/javascript">
    $('#today2').on('click', function() {
        date_from = this.value;
        $.ajax({
            url: 'index.php?route=common/events/getToday&date_from=' + date_from,
            dataType: 'html',
            success: function(data) {
                $('.events_list').html(data);
                // $('.line label').addClass('active');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('#date2').on('change', function() {
        date_from = $('#date1').val();
        date_to = this.value;
        $.ajax({
            url: 'index.php?route=common/events/getToday&date_from=' + date_from +'&date_to=' + date_to,
            dataType: 'html',
            success: function(data) {
                $('.events_list').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<script type="text/javascript">
    $('#any1').on('click', function() {
        $.ajax({
            url: 'index.php?route=common/events/getAll',
            dataType: 'html',
            success: function(data) {
                $('.events_list').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    $('#next3').on('click', function() {
        $.ajax({
            url: 'index.php?route=common/events/get7day',
            dataType: 'html',
            success: function(data) {
                $('.events_list').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    $('#next4').on('click', function() {
        $.ajax({
            url: 'index.php?route=common/events/get30day',
            dataType: 'html',
            success: function(data) {
                $('.events_list').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
</script>
<script>
    $(function(){
        $('.search').bind("change keyup input click", function() {
            if(this.value.length >= 1){
                $.ajax({
                    type: 'post',
                    url: "index.php?route=common/events/autocomplete_event",
                    data: {'search':this.value},
                    response: 'text',
                    success: function(data){
                        $(".search_result").html(data).fadeIn();
                    }
                })
            }
        })

        $(".search_result").hover(function(){
            $(".search").blur();
        })

        $(".search_result").on("click", "li", function(){
            s_user = $(this).find('.value').text();
            $(".search").val(s_user);
            $(".search_result").fadeOut();
        })

        $('form[name=Search]').submit(function(event){
            event.preventDefault();
            city = $(".search").val();
            $.ajax({
                url: 'index.php?route=common/events/event_result&city=' + city,
                dataType: 'html',
                success: function(htmlText) {
                    $('.events_list').html(htmlText);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
</script>

<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $(".date").datepicker({ dateFormat: 'yy-mm-dd' });
    })
</script>

<?php echo $footer; ?>