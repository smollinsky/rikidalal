		<!DOCTYPE html>
		<html ⚡ lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>Riki Dalal / Index</title>
			<meta content="Riki Dalal Main page" name="description">
			<link rel="canonical" href="<?php echo $canonical; ?>" >
			<meta content="Riki_Dalal " name="keywords">
			<meta name="HandheldFriendly" content="true">
			<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
			<meta property="og:title" content="Index">
			<meta property="og:url" content="">
			<meta property="og:description" content="Example description">
			<meta property="og:image" content="favicon.png">
			<meta property="og:image:type" content="image/png">
			<meta property="og:image:width" content="49">
			<meta property="og:image:height" content="59">
			<meta property="twitter:description" content="">
			<link rel="image_src" href="">
			<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
			<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
			<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
			<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
			<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
			<link rel="manifest" href="/manifest.json">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
			<meta name="theme-color" content="#bbbbbb">


			<style amp-boilerplate="">
				body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
			</style>
			<noscript>
				<style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
			</noscript>


			<style amp-custom>
				@font-face{font-family:Bell;src:url(catalog/view/theme/default/fonts/BellMT.eot);src:url(catalog/view/theme/default/fonts/BellMT.eot?#iefix) format('embedded-opentype'),url(catalog/view/theme/default/fonts/BellMT.woff2) format('woff2'),url(catalog/view/theme/default/fonts/BellMT.woff) format('woff'),url(catalog/view/theme/default/fonts/BellMT.ttf) format('truetype'),url(catalog/view/theme/default/fonts/BellMT.svg#catalog/view/theme/default/fonts/BellMT) format('svg');font-weight:400;font-style:normal}@font-face{font-family:Poppins;src:url(catalog/view/theme/default/fonts/Poppins-Light.eot);src:url(catalog/view/theme/default/fonts/Poppins-Light.eot?#iefix) format('embedded-opentype'),url(catalog/view/theme/default/fonts/Poppins-Light.woff2) format('woff2'),url(catalog/view/theme/default/fonts/Poppins-Light.woff) format('woff'),url(catalog/view/theme/default/fonts/Poppins-Light.ttf) format('truetype'),url(catalog/view/theme/default/fonts/Poppins-Light.svg#catalog/view/theme/default/fonts/Poppins-Light) format('svg');font-weight:300;font-style:normal}@font-face{font-family:Poppins;src:url(catalog/view/theme/default/fonts/Poppins-Medium.eot);src:url(catalog/view/theme/default/fonts/Poppins-Medium.eot?#iefix) format('embedded-opentype'),url(catalog/view/theme/default/fonts/Poppins-Medium.woff2) format('woff2'),url(catalog/view/theme/default/fonts/Poppins-Medium.woff) format('woff'),url(catalog/view/theme/default/fonts/Poppins-Medium.ttf) format('truetype'),url(catalog/view/theme/default/fonts/Poppins-Medium.svg#catalog/view/theme/default/fonts/Poppins-Medium) format('svg');font-weight:500;font-style:normal}


				a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,figcaption,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,ins,kbd,label,li,mark,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;vertical-align:baseline}img{padding:0}figure,footer,form{padding:0;border:0}fieldset,figure,footer,form,legend,menu{vertical-align:baseline}img,legend{margin:0}menu{border:0}:focus{outline:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:none}table{border-collapse:collapse;border-spacing:0}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration,input[type=search]::-webkit-search-results-button,input[type=search]::-webkit-search-results-decoration{-webkit-appearance:none;-moz-appearance:none}input[type=search]{-moz-appearance:none}audio,canvas,video{display:inline-block;max-width:100%}audio:not([controls]){display:none;height:0}[hidden]{display:none}html{font-size:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}a:focus{outline:thin dotted}a:active,a:hover{outline:0}img{border:0;-ms-interpolation-mode:bicubic;vertical-align:middle}fieldset,legend{border:0;padding:0}fieldset,figure,form{margin:0}legend{white-space:normal;}button,select{font-size:100%}button,input,select,textarea{margin:0;}button,input,select{vertical-align:baseline}textarea{font-size:100%}button,input{line-height:normal}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer;}button[disabled],html input[disabled]{cursor:default}input[type=checkbox],input[type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0;}input[type=search]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}textarea{overflow:auto;vertical-align:top}button,html,select,textarea{color:#222}::-moz-selection{background:#b3d4fc;text-shadow:none}::selection{background:#b3d4fc;text-shadow:none}textarea{resize:vertical}.chromeframe{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}@font-face{font-family:"Proba Pro";src:url(../fonts/ProbaPro/ProbaPro-Regular.woff2) format("WOFF2"),url(../fonts/ProbaPro/ProbaPro-Regular.woff) format("WOFF"),url(../fonts/ProbaPro/ProbaPro-Regular.svg) format("SVG");font-weight:400;font-style:normal}@font-face{font-family:"Proba Pro";src:url(../fonts/ProbaPro/ProbaPro-SemiBold.woff2) format("WOFF2"),url(../fonts/ProbaPro/ProbaPro-SemiBold.woff) format("WOFF"),url(../fonts/ProbaPro/ProbaPro-SemiBold.eot) format("EOT"),url(../fonts/ProbaPro/ProbaPro-SemiBold.svg) format("SVG");font-weight:600;font-style:normal}@font-face{font-family:"Proba Pro";src:url(../fonts/ProbaPro/ProbaPro-Bold.woff2) format("WOFF2"),url(../fonts/ProbaPro/ProbaPro-Bold.woff) format("WOFF"),url(../fonts/ProbaPro/ProbaPro-Bold.eot) format("EOT"),url(../fonts/ProbaPro/ProbaPro-Bold.svg) format("SVG");font-weight:700;font-style:normal}* html .page__wrapper,body,html{height:100%}.page__wrapper{min-height:100%;margin-bottom:-50px}.page__buffer{height:50px}.about_block{padding-top:72px}.about_block .header{text-align:center;margin-bottom:40px}.about_block .sub_header{text-align:center;width:100%;max-width:481px;margin:0 auto 55px}.about_block .wrapper_slider{background-image:url(catalog/view/theme/default/img/content/about.jpg);background-size:contain;background-repeat:no-repeat;background-position:left;padding:50px 0}.about_block .wrapper_slider .about_slider{padding:49px 39px 0;-webkit-box-shadow:0 0 20px 0 rgba(0,0,0,.06);box-shadow:0 0 20px 0 rgba(0,0,0,.06);background-color:#fff;max-width:585px;width:100%;margin-right:0;margin-left:auto}.about_block .wrapper_slider .about_slider .had{color:#686868;text-align:center;font-size:24px;font-weight:500;margin-bottom:38px}.about_block .wrapper_slider .about_slider .text{height:343px;overflow:hidden;color:#bbb;font-size:16px}.about_block .wrapper_slider .about_slider .slick-dots{bottom:20px;left:0;margin:0}.about_block .wrapper_slider .about_slider .slick-dots li,.our_client_block .entity .client_slider .slick-dots li{width:9px}.about_block .wrapper_slider .about_slider .slick-dots li.slick-active button:before,.our_client_block .entity .client_slider .slick-dots li.slick-active button:before{color:#c5a672}.about_block .wrapper_slider .about_slider .slick-dots li button:before{color:#dfdfdf}@media (max-width:1200px){.about_block .wrapper_slider .about_slider{max-width:430px}}@media (max-width:991px){.about_block{padding-top:50px}.about_block .header,.about_block .wrapper_slider .about_slider .had{margin-bottom:30px}.about_block .sub_header{max-width:306px;margin:0 auto 36px}.about_block .wrapper_slider{padding-top:186px;height:360px;margin-bottom:300px;background-position:center;background-size:cover}.about_block .wrapper_slider .about_slider{margin:0 auto;max-width:525px;padding:40px 9px 0 19px}}@media (max-width:525px){.about_block .wrapper_slider .about_slider{max-width:325px}.about_block .wrapper_slider .about_slider .text{height:450px}}.banner_block{margin-top:157px;padding:280px 0 268px;background-size:cover;background-repeat:no-repeat;background-position:center}.banner_block .entity{width:100%;max-width:703px;margin-right:0;margin-left:auto}.banner_block .entity .header{margin-bottom:48px;font-size:62px}.banner_block .entity .sub_header{margin-bottom:45px;padding-left:100px;max-width:460px;width:100%}@media (max-width:1200px){.banner_block .entity{max-width:625px}}@media (max-width:991px){.banner_block{margin-top:0;padding:0;min-height:350px;margin-bottom:226px;position:relative;background-position:left top}.banner_block .entity{max-width:362px;position:absolute;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);bottom:-216px;text-align:center}.banner_block .entity .sub_header{padding-left:0;margin-bottom:26px}.banner_block .entity .header{font-size:24px;margin:0 auto 23px;max-width:320px}.banner_block .btn_effect{margin:0 auto}}.contact_block{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row}.contact_block .__left{background-size:cover;background-repeat:no-repeat;background-position:center;background-image:url(catalog/view/theme/default/img/content/contact.jpg);width:50%}.contact_block .__right{width:50%;-webkit-box-shadow:0 0 20px 0 rgba(0,0,0,.06);box-shadow:0 0 20px 0 rgba(0,0,0,.06);background-color:#c5a672;padding:90px 0 100px 30px}.contact_block .__right .entity{max-width:555px;width:100%}.contact_block .__right .entity .header{margin-bottom:39px;color:#fff;text-align:center}.contact_block .__right .entity .sub_header{margin-bottom:36px;text-align:center;color:#fff}.contact_block .__right .entity form input{border:solid 1px #fff;outline:0;font-size:16px;font-weight:300;padding:11px 21px;background-color:transparent;height:48px;display:block;width:100%;color:#fff;max-width:270px;margin-bottom:19px}.contact_block .__right .entity form input::-webkit-input-placeholder,.contact_block .__right .entity form textarea::-webkit-input-placeholder{color:rgba(255,255,255,.5)}.contact_block .__right .entity form textarea,.flagship_block .entity form input{outline:0;font-size:16px;font-weight:300;padding:11px 21px;display:block;width:100%}.contact_block .__right .entity form textarea{resize:none;background-color:transparent;color:#fff;margin-bottom:31px;border:solid 1px #fff;height:80px}.contact_block .__right .entity form .btn_simple{margin:0 auto;border:0;outline:0;background-color:#fff;color:#c5a672;-webkit-box-shadow:0 0 0 2px #c5a672,0 0 0 3px #fff,0 0 0 4px #c5a672;box-shadow:0 0 0 2px #c5a672,0 0 0 3px #fff,0 0 0 4px #c5a672}.contact_block .__right .entity form .btn_simple:hover{background-color:transparent;color:#fff;-webkit-box-shadow:0 0 0 1px #fff,0 0 0 2px #c5a672,0 0 0 4px #fff;box-shadow:0 0 0 1px #fff,0 0 0 2px #c5a672,0 0 0 4px #fff}@media (max-width:1200px){.contact_block .__right{padding-right:30px}.contact_block .__right .entity .line{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.contact_block .__right .entity form input{max-width:inherit}}@media (max-width:991px){.contact_block .__left{display:none}.contact_block .__right .entity{margin:0 auto}.contact_block .__right{width:100%;padding:50px 15px 63px}.contact_block .__right .entity .header{margin-bottom:30px}.contact_block .__right .entity .sub_header{margin-bottom:26px}}.dress_style_blcok{padding-top:90px;overflow-x:hidden;-webkit-box-flex:1;-ms-flex:1 0 auto;flex:1 0 auto}.dress_style_blcok .header{text-align:center;margin-bottom:31px}.dress_style_blcok .sub_header{text-decoration:none;text-align:center;font-size:16px;margin-bottom:55px;display:block;-webkit-transition:all ease 400ms;transition:all ease 400ms}.dress_style_blcok .sub_header:hover,.dress_style_blcok .trend_list .trend__item p,footer .footer ._part .menu li a:hover,footer .footer ._part .social_link li a:hover p{-webkit-transition:all ease 400ms;transition:all ease 400ms;color:#c5a672}.dress_style_blcok .trend_list .wrapper{padding:0 15px}.dress_style_blcok .trend_list .slick-list{margin:0 -15px}.dress_style_blcok .trend_list .trend__item{display:block;width:100%;-webkit-transition:all ease 400ms;transition:all ease 400ms;text-decoration:none;text-align:center}.dress_style_blcok .trend_list .trend__item img{width:100%;height:auto;margin:0 auto 39px;-webkit-transition:all ease 400ms;transition:all ease 400ms}.dress_style_blcok .trend_list .trend__item p{padding-top:30px;text-align:center;color:#bbb;font-size:16px}.dress_style_blcok .trend_list .trend__item:hover img{-webkit-transform:scale(-1,1);transform:scale(-1,1);-webkit-transition:all ease 400ms;transition:all ease 400ms}.dress_style_blcok .trend_list .trend__item:hover p{color:#c5a672;-webkit-transition:all ease 400ms;transition:all ease 400ms}@media (max-width:991px){.dress_style_blcok .header{margin:0 auto 23px;max-width:222px}.dress_style_blcok .sub_header{font-size:14px;margin-bottom:36px}.dress_style_blcok{position:relative;padding-top:30px;padding-bottom:40px}.dress_style_blcok .trend_list .trend__item img{height:353px}.dress_style_blcok .trend_list .next,.dress_style_blcok .trend_list .prev{position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%);z-index:10}.dress_style_blcok .trend_list .prev{left:-20px}.dress_style_blcok .trend_list .next{right:-20px}.dress_style_blcok .trend_list{display:block;max-width:525px;margin:0 auto}.dress_style_blcok .trend_list .slick-dots{bottom:-40px;left:0;margin:0}.dress_style_blcok .trend_list .slick-dots li{width:9px}.dress_style_blcok .trend_list .slick-dots li.slick-active button:before{color:#c5a672}.dress_style_blcok .trend_list .slick-dots li button:before{color:#dfdfdf}}@media (max-width:600px){.dress_style_blcok .trend_list{max-width:325px}.dress_style_blcok .trend_list .prev{left:-13px}.dress_style_blcok .trend_list .next{right:-13px}}.flagship_block{margin-top:90px;padding-top:450px;background-image:url(catalog/view/theme/default/img/content/flagship.jpg);background-size:cover;background-repeat:no-repeat;background-position:center;margin-bottom:156px}.flagship_block .entity{width:100%;max-width:760px;-webkit-box-shadow:0 0 20px 0 rgba(0,0,0,.06);box-shadow:0 0 20px 0 rgba(0,0,0,.06);background-color:#fff;opacity:.95;padding:50px 40px 60px;position:relative;z-index:10;margin:0 auto -156px}.flagship_block .entity .header{margin-bottom:26px;text-align:center}.flagship_block .entity .sub_header{margin-bottom:45px;text-align:center}.flagship_block .entity form{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin-bottom:50px}.flagship_block .entity form input{border:solid 1px #bbb;height:50px;max-width:450px}.flagship_block .entity form input::-webkit-input-placeholder,footer .footer ._part form input::-webkit-input-placeholder{color:#e0e0e0}.flagship_block .entity form .btn_simple,footer .footer ._part form .btn_simple{border:0;outline:0}.flagship_block .entity .btn_effect{margin:0 auto}@media (max-width:991px){.flagship_block{margin-top:0;padding-top:263px}.flagship_block .entity{max-width:525px;padding:40px 20px 60px}.flagship_block .entity .sub_header{margin-bottom:30px}.flagship_block .entity form{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;margin-bottom:30px}.flagship_block .entity form input{margin-bottom:22px}}@media (max-width:525px){.flagship_block .entity{max-width:325px}}footer{border-top:1px solid #e0e0e0;margin:100px 0 20px}footer .footer{padding-top:40px;margin-bottom:66px}footer .footer ._part{width:33.33%}footer .footer ._part .had{color:#686868;margin-bottom:30px;font-size:16px}footer .footer ._part .menu li a{text-decoration:none;display:block;margin-bottom:10px;font-size:16px;color:#bbb;-webkit-transition:all ease 400ms;transition:all ease 400ms}.line,footer .footer,footer .footer ._part .social_link li a{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row}footer .footer ._part .social_link li a{text-decoration:none;margin-bottom:10px;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-transition:all ease 400ms;transition:all ease 400ms}footer .footer ._part .social_link li a p{font-size:16px;padding-left:15px;color:#bbb;-webkit-transition:all ease 400ms;transition:all ease 400ms}footer .footer ._part .social_link li a svg{width:15px;height:16px}footer .footer ._part .social_link li a svg path{fill:#bbb;-webkit-transition:all ease 400ms;transition:all ease 400ms}footer .footer ._part .social_link li a:hover svg path,header ._top_header .social_link ._link:hover svg path,header ._top_part .social_link ._link:hover svg path{-webkit-transition:all ease 400ms;transition:all ease 400ms;fill:#c5a672}footer .footer ._part form input{border:solid 1px #bbb;outline:0;font-size:16px;font-weight:300;padding:11px 21px;height:48px;display:block;width:100%;max-width:270px;margin-bottom:15px}footer .copyright{color:#bbb;font-size:16px;text-align:center}footer .copyright a{font-size:16px;color:#c5a672;text-decoration:none}@media (max-width:767px){footer .footer{-ms-flex-wrap:wrap;flex-wrap:wrap}footer .footer ._part{width:50%}footer .footer ._part:last-child{padding-top:30px;width:100%}footer .footer ._part form input{max-width:inherit}footer .footer ._part form button{margin-left:3px}footer .footer ._part .menu li a{font-size:14px}}.line{-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}.header{font-family:"Bell",sans-serif;font-size:48px;color:#c5a672}.sub_header,input{font-size:16px;color:#bbb}.btn_effect,.btn_simple{width:100%;display:block;text-decoration:none}.btn_simple,.btn_simple:hover{-webkit-transition:all 400ms ease;transition:all 400ms ease}.btn_simple{font-size:16px;text-align:center;padding:11px 0;max-width:194px;height:44px;color:#fff;background-color:#c5a672;-webkit-box-shadow:0 0 0 1px #fff,0 0 0 2px #fff,0 0 0 3px #c5a672;box-shadow:0 0 0 1px #fff,0 0 0 2px #fff,0 0 0 3px #c5a672}.btn_simple:hover{background-color:#fff;color:#c5a672;-webkit-box-shadow:0 0 0 1px #c5a672,0 0 0 2px #fff,0 0 0 3px #c5a672;box-shadow:0 0 0 1px #c5a672,0 0 0 2px #fff,0 0 0 3px #c5a672}.btn_effect{margin-left:100px;max-width:192px;background-color:transparent;height:42px;margin-right:3px;position:relative;-webkit-box-shadow:0 0 0 1px #bbb,0 0 0 3px #fff,0 0 0 4px #bbb;box-shadow:0 0 0 1px #bbb,0 0 0 3px #fff,0 0 0 4px #bbb}.btn_effect .text{color:#bbb;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);font-size:16px;white-space:nowrap}.btn_effect:hover .text{color:#c5a672;-webkit-transition:all ease 700ms;transition:all ease 700ms}.btn_effect .horz:after,.btn_effect .horz:before,.btn_effect:hover .horz:after,.btn_effect:hover .horz:before{-webkit-transition:width .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms;transition:width .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms;width:100%}.btn_effect:hover .vert:after,.btn_effect:hover .vert:before{-webkit-transition:height .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms;transition:height .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms;height:100%}.btn_effect .horz{width:100%}.btn_effect .horz:after,.btn_effect .horz:before{content:"";display:block;width:0;height:2px;position:absolute;border-top:solid 1px #c5a672;border-bottom:solid 1px #c5a672}.btn_effect .horz:after{top:-4px;left:0}.btn_effect .horz:before{bottom:-4px;right:0}.btn_effect .vert{height:100%}.btn_effect .vert:after,.btn_effect .vert:before{content:"";display:block;width:2px;height:0;position:absolute;border-right:solid 1px #c5a672;border-left:solid 1px #c5a672;-webkit-transition:height .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms;transition:height .7s cubic-bezier(.165,.84,.44,1),opacity 10ms linear 690ms}.btn_effect .vert:after{left:-4px;bottom:0}.btn_effect .vert:before{top:0;right:-4px}header .mobile__header{display:none}header{-webkit-box-flex:1;-ms-flex:1 0 auto;flex:1 0 auto;position:relative;z-index:10000000000}header ._top_header,header ._top_part,header .desktop__header{width:100%;-webkit-box-shadow:0 0 15px 0 rgba(0,0,0,.1);box-shadow:0 0 15px 0 rgba(0,0,0,.1)}header .desktop__header{position:fixed;left:0;top:0;background-color:#fff}header ._top_header,header ._top_part{padding:15px 0 13px;background-color:#262829}header ._top_header .line,header ._top_part .line{-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start}header ._top_header .req,header ._top_part .req{color:#efefef;letter-spacing:.1px;text-transform:uppercase;text-decoration:none;-webkit-transition:all ease 400ms;transition:all ease 400ms}.insta_block .sub_header:hover,header ._bottom_header .menu li a:hover,header ._top_header .req:hover,header ._top_part .req:hover{-webkit-transition:all ease 400ms;transition:all ease 400ms;color:#c5a672}header ._top_header .social_link,header ._top_part .social_link{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start}header ._top_header .social_link ._link,header ._top_part .social_link ._link{margin-right:19px;display:block;height:16px;text-decoration:none}header ._top_header .social_link ._link:last-child,header ._top_part .social_link ._link:last-child{margin-right:0}header ._top_header .social_link ._link svg path,header ._top_part .social_link ._link svg path{fill:#efefef;-webkit-transition:all ease 400ms;transition:all ease 400ms}header ._bottom_header,header ._bottom_header .menu{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}header ._bottom_header{padding:25px 0;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.nav__show-menu ul li a,header ._bottom_header .logo{display:block;text-decoration:none}header ._bottom_header .logo img{width:49px;height:59px}header ._bottom_header .menu{padding-left:0;margin-right:16px;list-style:none;max-width:780px;width:100%}.insta_block .sub_header,header ._bottom_header .menu li a{text-decoration:none;display:block;-webkit-transition:all ease 400ms;transition:all ease 400ms}header ._bottom_header .menu li a{color:#bbb;text-transform:uppercase}@media (max-width:1200px){header ._bottom_header .menu{margin:0 20px}}@media (max-width:991px){.header{font-size:24px}.sub_header{font-size:14px}header .desktop__header{display:none}header .mobile__header{display:block;position:relative}.sendwich{position:relative;z-index:9;width:20px;height:17px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}span.sendwich-btn{width:20px;height:1px;background-color:#fff;z-index:10;display:block;-webkit-transition:all 300ms ease;transition:all 300ms ease}span.sendwich-btn:after,span.sendwich-btn:before{display:block;position:absolute;width:20px;height:1px;background-color:#fff;content:''}span.sendwich-btn:after{top:0}span.sendwich-btn.active,span.sendwich-btn:after,span.sendwich-btn:before{-webkit-transition:all 300ms ease;transition:all 300ms ease}span.sendwich-btn:before{bottom:0}span.sendwich-btn.active{background:0 0}span.sendwich-btn.active:after,span.sendwich-btn.active:before{-webkit-transform:translate(-50%,-50%) rotate(45deg);transform:translate(-50%,-50%) rotate(45deg);top:50%;left:50%;-webkit-transition:all 300ms ease;transition:all 300ms ease}span.sendwich-btn.active:after{-webkit-transform:translate(-50%,-50%) rotate(-45deg);transform:translate(-50%,-50%) rotate(-45deg)}header .mobile__header ._fixed_part{position:fixed;left:0;top:0;width:100%;-webkit-box-shadow:0 0 15px 0 rgba(0,0,0,.1);box-shadow:0 0 15px 0 rgba(0,0,0,.1)}header .mobile__header ._bottom_part{text-align:center;margin-top:45px}header .mobile__header ._bottom_part .mobile_logo{padding:15px 0;max-width:173px;width:100%;height:auto}.menu.amp{ padding-left:15px;border-top:1px solid #303030;display:flex;height:100vh;flex-direction:column;transition:all ease 400ms;background-color:#262829}.menu.amp.active{-webkit-transition:all ease 400ms;transition:all ease 400ms;max-height:700px}.menu.amp li{line-height:.05;padding:15px 0}.menu.amp li:last-child{flex:0 0 auto;}.menu.amp li:nth-last-child(2){flex:1 0 auto;padding-bottom:0}.menu.amp li a{color:#efefef;display:block;text-decoration:none;text-transform:uppercase;}.menu.amp li .btn_simple{-webkit-box-shadow:0 0 0 1px #000,0 0 0 2px #000,0 0 0 3px #c5a672;box-shadow:0 0 0 1px #000,0 0 0 2px #000,0 0 0 3px #c5a672;-webkit-transition:all ease 400ms;transition:all ease 400ms}.menu.amp li .btn_simple:hover{-webkit-transition:all ease 400ms;transition:all ease 400ms;background-color:#000;-webkit-box-shadow:0 0 0 1px #c5a672,0 0 0 2px #c5a672,0 0 0 3px #000;box-shadow:0 0 0 1px #c5a672,0 0 0 2px #c5a672,0 0 0 3px #000}}.insta_block{padding-top:90px;width:100%}.insta_block .header{text-align:center;margin-bottom:26px}.insta_block .sub_header{text-align:center;margin:0 auto 55px}.insta_block .insta_slider{overflow-x:hidden}.insta_block .insta_slider .slick-list{margin:0 -2px}.insta_block .insta_slider .slider__item{margin:0 2px;-webkit-transition:all ease 400ms;transition:all ease 400ms}.insta_block .insta_slider .slider__item:hover{-webkit-transition:all ease 400ms;transition:all ease 400ms;-webkit-transform:scale(1.1);transform:scale(1.1)}.insta_block .insta_slider .slider__item .img svg,.insta_block .insta_slider .slider__item:hover .img svg{-webkit-transition:all ease 400ms;transition:all ease 400ms;bottom:30px;left:15px}.insta_block .insta_slider .slider__item .img svg path,.insta_block .insta_slider .slider__item:hover .img svg path{fill:#c5a672;-webkit-transition:all ease 400ms;transition:all ease 400ms}.insta_block .insta_slider .slider__item .img{height:320px;width:100%;background-size:cover;background-position:center;background-repeat:no-repeat;position:relative}.insta_block .insta_slider .slider__item .img svg{position:absolute;left:10px;bottom:10px}.insta_block .insta_slider .slider__item .img svg path{fill:#fff}@media (max-width:991px){.insta_block{padding-top:50px}.insta_block .sub_header{margin-bottom:37px}.insta_block .insta_slider .slider__item .img{height:161px}.insta_block .insta_slider .slick-list{max-width:93%;overflow:visible}}.container-fluide{padding:0 15px}.container{margin:0 auto;width:1170px}@media (max-width:1200px){.container{width:970px}}@media (max-width:992px){.container{max-width:940px;padding:0 15px;width:100%}}@media (max-width:768px){.container{width:100%}}.nav__show-menu{padding:30px 0;-webkit-box-flex:1;-ms-flex:1 1 auto;flex:1 1 auto}.nav__show-menu h1{color:#234161;font-size:36px;margin-bottom:30px}.nav__show-menu ul li a{font-size:18px;color:#4ea7dc;margin-bottom:10px}#modal-gov .wrap,.main-logo-dark,.main-logo-light{display:-webkit-box;display:-ms-flexbox;display:flex}.main-logo-dark,.main-logo-light{text-decoration:none}.main-logo-dark .icon-flag,.main-logo-light .icon-flag{width:10px;height:40px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;-ms-flex-direction:column-reverse;flex-direction:column-reverse;margin-right:10px}.main-logo-dark .icon-flag .yellow,.main-logo-light .icon-flag .yellow{background-color:#ffe358;width:10px;height:20px}.main-logo-dark .icon-flag .blue,.main-logo-light .icon-flag .blue{background-color:#2669e3;width:10px;height:20px}.main-logo-dark .wrap-text,.main-logo-light .wrap-text{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}.main-logo-light .wrap-text .one{color:#337ab7}.main-logo-dark .wrap-text .one,.main-logo-light .wrap-text .one{font-weight:700;line-height:.5;font-size:154%}.main-logo-dark .wrap-text .light,.main-logo-light .wrap-text .light{font-weight:100;color:#aab3c6;margin-top:10px;font-size:.8em;letter-spacing:.8px;white-space:nowrap}.main-logo-dark .wrap-text .one{color:#fff}#modal-gov{background-color:#31486c;max-width:760px;position:absolute;z-index:1050;display:none}#modal-gov .wrap{padding:30px 0 30px 60px;-ms-flex-wrap:wrap;flex-wrap:wrap}#modal-gov .wrap .wrap-link{margin-right:40px;padding-left:0;margin-bottom:30px}#modal-gov .wrap .wrap-link li{list-style-type:none}#modal-gov .wrap .wrap-link li a{color:#fff;border-bottom:1px solid transparent;-webkit-transition:all .2s ease-in-out;transition:all .2s ease-in-out;text-decoration:none;font-weight:700;line-height:30px}#modal-gov .wrap .wrap-link li a:hover{text-decoration:underline}#modal-gov .close{right:15px;top:15px;width:16px;height:16px;position:absolute;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDM1LjQxMyAzNS40MTMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDM1LjQxMyAzNS40MTM7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0yMC41MzUsMTcuMjk0TDM0LjAwMiwzLjgyN2MwLjc4MS0wLjc4MSwwLjc4MS0yLjA0NywwLTIuODI4Yy0wLjc4LTAuNzgxLTIuMDQ4LTAuNzgxLTIuODI4LDBMMTcuNzA3LDE0LjQ2Nkw0LjI0MiwwLjk5OSAgICBjLTAuNzgtMC43ODEtMi4wNDctMC43ODEtMi44MjgsMHMtMC43ODEsMi4wNDcsMCwyLjgyOGwxMy40NjUsMTMuNDY3TDAuNTg2LDMxLjU4N2MtMC43ODEsMC43ODEtMC43ODEsMi4wNDcsMCwyLjgyOCAgICBjMC4zOSwwLjM5MSwwLjkwMiwwLjU4NiwxLjQxNCwwLjU4NnMxLjAyNC0wLjE5NSwxLjQxNC0wLjU4NmwxNC4yOTMtMTQuMjkzTDMyLDM0LjQxNWMwLjM5MSwwLjM5MSwwLjkwMiwwLjU4NiwxLjQxNCwwLjU4NiAgICBzMS4wMjMtMC4xOTUsMS40MTQtMC41ODZjMC43ODEtMC43ODEsMC43ODEtMi4wNDcsMC0yLjgyOEwyMC41MzUsMTcuMjk0eiIgZmlsbD0iI0ZGRkZGRiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=) center no-repeat;background-size:cover}#modal-gov .close:hover{cursor:pointer}@media only screen and (max-width:767px){#modal-gov{margin-left:-15px}#modal-gov .wrap{padding:15px 0 15px 30px}#modal-gov .wrap .wrap-link{margin-top:10px;margin-bottom:15px}#modal-gov .wrap .wrap-link li a{line-height:24px}}.modal-background{background-color:#000;opacity:.5;position:fixed;top:0;right:0;bottom:0;left:0;z-index:1000;display:none}*{font-family:'Poppins',sans-serif;-webkit-box-sizing:border-box;box-sizing:border-box;line-height:1.43;font-size:14px;font-weight:300}menu{margin:0;padding:0}.flex{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;min-height:100vh}.stick{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto}.our_client_block{padding:90px 0 130px;background-size:cover;background-repeat:no-repeat;background-position:center}.our_client_block .entity{width:100%;max-width:829px;margin:0 auto}.our_client_block .entity .header{margin-bottom:37px;text-align:center}.our_client_block .entity .client_slider .client__item .sub_header{margin-bottom:25px;max-width:829px;width:100%;text-align:center}.our_client_block .entity .client_slider .client__item .profile{max-width:200px;margin:0 auto;width:100%}.our_client_block .entity .client_slider .client__item .profile .img{width:80px;height:80px;border-radius:50%;background-size:cover;background-position:center;background-repeat:no-repeat}.our_client_block .entity .client_slider .client__item .profile .content .name{color:#686868;margin-bottom:14px;font-size:16px;font-weight:500}.our_client_block .entity .client_slider .client__item .profile .content .date{color:#c5a672;font-size:16px}.our_client_block .entity .client_slider .slick-dots{bottom:-80px;left:0;margin:0}.our_client_block .entity .client_slider .slick-dots li button:before{color:#dfdfdf}@media (max-width:991px){.our_client_block .entity .header{margin-bottom:33px}.our_client_block .entity .client_slider .client__item .sub_header{max-width:320px;margin:0 auto 25px}.our_client_block{padding:50px 0 90px}}.our_latest_block{padding:90px 0 0;background-size:cover;background-repeat:no-repeat;background-position:center}.our_client_block .entity .client_slider .client__item .profile,.our_latest_block .entity{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.our_latest_block .entity .__left{max-width:377px;width:100%}.our_latest_block .entity .__left .header{margin-right:0;margin-left:auto;margin-bottom:39px;text-align:right;font-size:48px;max-width:331px}.our_latest_block .entity .__left .sub_header{text-align:right;margin-bottom:45px;width:100%;line-height:1.75}.our_latest_block .entity .__left .btn_effect{margin-right:0;margin-left:auto}.our_latest_block .entity .__right{max-width:680px;width:100%}.our_latest_block .entity .__right .img{height:550px;background-size:cover;background-repeat:no-repeat;background-position:center}@media (max-width:1200px){.our_latest_block .entity .__right{max-width:530px}}@media (max-width:991px){.our_latest_block .entity{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;position:relative;padding-bottom:80px}.our_latest_block .entity .__left .sub_header{text-align:center}.our_latest_block .entity .__left .btn_effect{position:absolute;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);bottom:0}.our_latest_block .entity .__left .header{font-size:24px;text-align:center;margin:0 auto 30px;max-width:164px}.our_latest_block{margin-bottom:26px;padding-top:50px}.our_latest_block .entity .__right .img{height:450px}}@media (max-width:767px){.our_latest_block .entity .__right .img{height:280px;max-width:325px;margin:0 auto}}.trend_block{padding-top:72px;overflow:hidden;-webkit-box-flex:1;-ms-flex:1 0 auto;flex:1 0 auto}.trend_block .header{text-align:center;margin-bottom:60px}.trend_block .trend_list .wrapper{padding:0 15px}.trend_block .trend_list .slick-list{margin:0 -15px}.trend_block .trend_list .trend__item{width:100%;display:block;height:550px;background-size:cover;background-position:center;background-repeat:no-repeat;overflow:hidden;padding:0 15px;-webkit-box-shadow:0 0 15px 0 rgba(0,0,0,.1);box-shadow:0 0 15px 0 rgba(0,0,0,.1);position:relative;-webkit-transition:all ease 400ms;transition:all ease 400ms}.trend_block .trend_list .trend__item:hover{-webkit-box-shadow:0 0 20px 0 rgba(0,0,0,.06);box-shadow:0 0 20px 0 rgba(0,0,0,.06);-webkit-transition:all ease 400ms;transition:all ease 400ms;-webkit-transform:scale(1.1);transform:scale(1.1)}.trend_block .trend_list .trend__item ._hover,.trend_block .trend_list .trend__item:hover ._hover{-webkit-transition:all ease 400ms;transition:all ease 400ms;left:-1px}.trend_block .trend_list .trend__item ._hover{background-color:#fff;padding:30px 20px;max-width:300px;width:100%;position:absolute;left:-100%;bottom:40px}.trend_block .trend_list .trend__item ._hover .had{color:#686868;font-size:24px;font-weight:500;margin-bottom:19px}.trend_block .trend_list .trend__item ._hover .text{color:#bbb;font-size:16px;line-height:1.75;letter-spacing:.2px}@media (max-width:991px){.trend_block .trend_list .trend__item{width:100%;background-position:center;height:485px;margin:0 auto}.trend_block .trend_list{display:block;max-width:525px;margin:0 auto}.trend_block{position:relative;padding-top:90px}.trend_block .trend_list .next,.trend_block .trend_list .prev{position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%);z-index:10}.trend_block .trend_list .prev{left:-20px}.trend_block .trend_list .next{right:-20px}.trend_block .header{margin-bottom:40px}.trend_block .trend_list .trend__item ._hover .had{font-size:20px}.trend_block .trend_list .trend__item:hover{-webkit-transform:none;transform:none}.trend_block .trend_list .trend__item ._hover,.trend_block .trend_list .trend__item:hover ._hover{left:0;bottom:20px;max-width:230px}.trend_block .trend_list .trend__item ._hover .text{font-size:14px}.trend_block .trend_list .slick-dots{bottom:-60px;left:0;margin:0}.trend_block .trend_list .slick-dots li{width:9px}.trend_block .trend_list .slick-dots li.slick-active button:before{color:#c5a672}.trend_block .trend_list .slick-dots li button:before{color:#dfdfdf}.trend_block .slick-dotted.slick-slider{margin-bottom:60px}}@media (max-width:768px){.trend_block .trend_list .trend__item{max-width:325px}.trend_block .trend_list .trend__item ._hover{max-width:300px}}@media (max-width:600px){.trend_block{padding-top:110px}.trend_block .trend_list .trend__item ._hover,.trend_block .trend_list .trend__item:hover ._hover{max-width:265px}.trend_block .trend_list .prev{left:-13px}.trend_block .trend_list .next{right:-13px}}
					.amp-carousel-button-prev{background:url("catalog/view/theme/default/img/minified-svg/prev.svg");width:10px;height:20px;background-position: center;background-size:cover; position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);z-index: 10; left:0;opacity: 1;visibility: }.amp-carousel-button-next{background:url("catalog/view/theme/default/img/minified-svg/next.svg");width:10px;height:20px;background-position: enter;background-size: cover; position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);z-index: 10; right:0;left:inherit;opacity: 1;}.about_block .amp-carousel-button-next{display: none;}.about_block .amp-carousel-button-prev{display: none;}.our_client_block .amp-carousel-button-next{display: none;}.our_client_block .amp-carousel-button-prev{display: none;}

			</style>
			<script async src="https://cdn.ampproject.org/v0.js"></script>
			<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
			<script custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js" async></script>
			<script custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js" async></script>
			<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
			<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
			<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

		</head>
		<body class="flex">
		<header>
			<div class="desktop__header">
				<div class="_top_header">
					<div class="container">
						<div class="line"><a class="req" href="/index.php?route=common/arequest">Request an appointment</a>
							<div class="social_link">
								<a class="_link" href="<?php echo $social_facebook; ?>">
									<svg xmlns="http://www.w3.org/2000/svg" width="10" height="16">
										<path fill-rule="evenodd" fill="#BBB" d="M8.699,0.002 L6.780,-0.001 C4.624,-0.001 4.000,0.757 4.000,2.999 L4.000,5.999 L1.000,5.999 C0.833,5.999 1.000,5.529 1.000,5.699 L1.000,8.164 C1.000,8.334 0.833,7.999 1.000,7.999 L4.000,7.999 L4.000,14.999 C4.000,15.168 3.833,14.999 4.000,14.999 L6.050,14.999 C6.216,14.999 6.000,15.168 6.000,14.999 L6.000,7.999 L9.000,7.999 C9.167,7.999 8.909,8.334 8.909,8.164 L9.000,5.999 C9.000,5.918 9.056,6.057 9.000,5.999 C8.943,5.942 9.080,5.999 9.000,5.999 L6.000,5.999 L6.000,2.999 C6.000,2.305 6.078,2.593 6.000,1.999 L9.000,1.999 C9.166,1.999 9.000,2.168 9.000,1.999 L9.000,0.308 C9.000,0.139 8.865,0.002 8.699,0.002 Z"
										/>
									</svg>
								</a>
								<a class="_link" href="<?php echo $social_pinterest; ?>">
									<svg xmlns="http://www.w3.org/2000/svg" width="12" height="15">
										<path fill-rule="evenodd" fill="#BBB" d="M4.522,9.920 C4.150,11.985 3.697,13.965 2.354,14.999 C1.940,11.878 2.963,9.534 3.438,7.045 C2.628,5.599 3.536,2.685 5.244,3.404 C7.347,4.286 3.423,8.783 6.057,9.345 C8.807,9.932 9.930,4.282 8.225,2.445 C5.761,-0.207 1.053,2.384 1.632,6.182 C1.773,7.111 2.677,7.393 1.993,8.674 C0.416,8.303 -0.055,6.984 0.006,5.225 C0.104,2.344 2.445,0.328 4.793,0.050 C7.762,-0.303 10.549,1.206 10.934,4.170 C11.367,7.516 9.594,11.139 6.418,10.878 C5.558,10.807 5.196,10.355 4.522,9.920 Z"
										/>
									</svg>
								</a>
								<a class="_link" href="<?php echo $social_instagram; ?>">
									<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
										<path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
										/>
									</svg>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="_bottom_header">
						<a class="logo" href="<?php echo $home; ?>">
							<amp-img src="<?php echo $logo; ?>" srcset="<?php echo $logo; ?>, <?php echo $logo; ?>" width="49" height="59" alt="logo">
						</a>
						<ul class="menu">
							<?php if ($categories) { ?>
							<?php foreach ($categories as $category) { ?>
							<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
							<?php } ?>
							<?php } ?>
						</ul><a class="btn_simple" href="#"><?php echo $text_contact1; ?></a>
					</div>
				</div>
			</div>
			<div class="mobile__header">
				<div class="_fixed_part">
					<div class="_top_part">
						<div class="container">
							<div class="line">
								<div class="sendwich" on="tap:sidebar.toggle" tabindex="0"><span class="sendwich-btn"></span>
								</div>
								<div class="social_link">
									<a class="_link" href="<?php echo $social_facebook; ?>">
										<svg xmlns="http://www.w3.org/2000/svg" width="10" height="16">
											<path fill-rule="evenodd" fill="#BBB" d="M8.699,0.002 L6.780,-0.001 C4.624,-0.001 4.000,0.757 4.000,2.999 L4.000,5.999 L1.000,5.999 C0.833,5.999 1.000,5.529 1.000,5.699 L1.000,8.164 C1.000,8.334 0.833,7.999 1.000,7.999 L4.000,7.999 L4.000,14.999 C4.000,15.168 3.833,14.999 4.000,14.999 L6.050,14.999 C6.216,14.999 6.000,15.168 6.000,14.999 L6.000,7.999 L9.000,7.999 C9.167,7.999 8.909,8.334 8.909,8.164 L9.000,5.999 C9.000,5.918 9.056,6.057 9.000,5.999 C8.943,5.942 9.080,5.999 9.000,5.999 L6.000,5.999 L6.000,2.999 C6.000,2.305 6.078,2.593 6.000,1.999 L9.000,1.999 C9.166,1.999 9.000,2.168 9.000,1.999 L9.000,0.308 C9.000,0.139 8.865,0.002 8.699,0.002 Z"
											/>
										</svg>
									</a>
									<a class="_link" href="<?php echo $social_pinterest; ?>">
										<svg xmlns="http://www.w3.org/2000/svg" width="12" height="15">
											<path fill-rule="evenodd" fill="#BBB" d="M4.522,9.920 C4.150,11.985 3.697,13.965 2.354,14.999 C1.940,11.878 2.963,9.534 3.438,7.045 C2.628,5.599 3.536,2.685 5.244,3.404 C7.347,4.286 3.423,8.783 6.057,9.345 C8.807,9.932 9.930,4.282 8.225,2.445 C5.761,-0.207 1.053,2.384 1.632,6.182 C1.773,7.111 2.677,7.393 1.993,8.674 C0.416,8.303 -0.055,6.984 0.006,5.225 C0.104,2.344 2.445,0.328 4.793,0.050 C7.762,-0.303 10.549,1.206 10.934,4.170 C11.367,7.516 9.594,11.139 6.418,10.878 C5.558,10.807 5.196,10.355 4.522,9.920 Z"
											/>
										</svg>
									</a>
									<a class="_link" href="<?php echo $social_instagram; ?>">
										<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
											<path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
											/>
										</svg>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="_bottom_part">
					<amp-img class="mobile_logo" src="<?php echo $logo1; ?>" width="173" height="72" alt="logo">
				</div>
			</div>
		</header>
		<amp-sidebar id="sidebar" style="width: 100%;"  layout="nodisplay" side="left">

			<ul class="menu amp">
				<li><div class="sendwich" tabindex="1" on="tap:sidebar.toggle" role="close"><span class="sendwich-btn active"></span></li>
				<?php if ($categories) { ?>
				<?php foreach ($categories as $category) { ?>
				<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>
				<?php } ?>
				<li><a class="btn_simple" href="#"><?php echo $text_contact1; ?></a>
				</li>
			</ul>

		</amp-sidebar>
		<?php foreach ($banners_main as $banner) { ?>
		<section class="banner_block" id="banner<?php echo $module; ?>" style="background-image:url('<?php echo $banner['image']; ?>')">
			<div class="container">
				<div class="entity">
					<h1 class="header"><?php echo $banner['title']; ?></h1>
					<p class="sub_header"><?php echo $banner['title2']; ?></p><a class="btn_effect" href="<?php echo $banner['link']; ?>"><span class="vert"></span><span class="horz"></span><span class="text"><?php echo $text_collection; ?></span></a>
				</div>
			</div>
		</section>
		<?php } ?>
		<section class="about_block">
			<div class="container">
				<h3 class="header"><?php echo $heading_title; ?></h3>
				<p class="sub_header"><?php echo $heading_text; ?></p>
				<div class="wrapper_slider">
					<div class="about_slider" style="height:514px">
						<amp-carousel  style="width: 100%;height: 447px;margin: 0 auto; " width="525" height="800" autoplay loop
									   delay="4000" layout="responsive" type="slides" >
							<?php foreach ($banners_about as $banner_about) { ?>
							<div>
								<div class="slider__item" >
									<p class="had"><?php echo $banner_about['title']; ?></p>
									<p class="text"><?php echo $banner_about['title2']; ?></p>
								</div>
							</div>
							<?php } ?>
							<div>
							</div>
						</amp-carousel>
					</div>
				</div>
			</div>
		</section>
		<div class="trend_block">
			<div class="container">
				<h3 class="header">Latest Trends</h3>
				<div class="trend_list" >
					<amp-carousel style="max-width: 425px; width:100%;height: 485px;margin: 0 auto; " width="1000" height="800" autoplay loop controls	delay="4000" layout="responsive" type="slides" >
						<?php foreach ($products as $product) { ?>
						<div class="wrapper">
							<a class="trend__item" href="<?php echo $product['href']; ?>" style="background-image:url('<?php echo $product['thumb']; ?>')">
								<div class="_hover">
									<p class="had"><?php echo $product['name']; ?></p>
									<p class="text"><?php echo $product['description']; ?></p>
								</div>
							</a>
						</div>
						<?php } ?>
					</amp-carousel>
				</div>
			</div>
		</div>
		<section class="our_latest_block">
			<div class="container">
				<?php foreach ($banners_our as $banner) { ?>
				<div class="entity">
					<div class="__left">
						<h1 class="header"><?php echo $banner['title']; ?></h1>
						<p class="sub_header"><?php echo $banner['title2']; ?></p><a class="btn_effect" href="<?php echo $banner['link']; ?>"><span class="vert"></span><span class="horz"></span><span class="text">View collection</span></a>
					</div>
					<div class="__right">
						<div class="img" style="background-image:url('<?php echo $banner['image']; ?>')"></div>
					</div>
				</div>
				<?php } ?>
			</div>
		</section>
		<section class="dress_style_blcok">
			<div class="container">
				<h3 class="header"><?php echo $heading_title_img; ?></h3><a class="sub_header" href="#"><?php echo $heading_texts_img; ?></a>
				<div class="trend_list">
					<amp-carousel style="max-width: 425px; width:100%;height: 485px;margin: 0 auto; "   width="507" height="650" autoplay loop controls
								  delay="4000" layout="responsive" type="slides" >
						<?php foreach ($categoriess as $category) { ?>
						<div class="wrapper">
							<a class="trend__item" href="<?php echo $category['href']; ?>">
								<amp-img src="<?php echo $category['thumb']; ?>" srcset="<?php echo $category['thumb']; ?>, <?php echo $category['thumb']; ?>" style="max-width: <?php echo $category['column']; ?>px;" width="217" height="353" alt="logo"></amp-img>
								<p><?php echo $category['name']; ?></p>
							</a>
						</div>
						<?php } ?>
					</amp-carousel>
				</div>
			</div>
		</section>
		<section class="flagship_block">
			<div class="container">
				<div class="entity">
					<h1 class="header"><?php echo $text_search1; ?></h1>
					<p class="sub_header"><?php echo $text_search2; ?></p>
					<form method="post" action-xhr="https://example.com/subscribe">
						<input placeholder="<?php echo $text_search3; ?>">
						<button class="btn_simple" type="submit"><?php echo $text_search4; ?></button>
					</form><a class="btn_effect" href="#"><span class="vert"></span><span class="horz"></span><span class="text"><?php echo $text_search5; ?></span></a>
				</div>
			</div>
		</section>
		<section class="our_client_block">
			<div class="container">
				<div class="entity">
					<h1 class="header"><?php echo $heading_title_clients; ?></h1>
					<div class="client_slider" >
						<amp-carousel style="max-width: 425px; width:100%;height: 220px;margin: 0 auto; "  width="507" height="300" autoplay loop
									  delay="4000" layout="responsive" type="slides" >
							<?php foreach ($banners_clients as $banner) { ?>
							<div class="client__item">
								<p class="sub_header"><?php echo $banner['title2']; ?></p>
								<div class="profile">
									<div class="img" style="background-image:url('<?php echo $banner['image']; ?>')"></div>
									<div class="content">
										<p class="name"><?php echo $banner['title']; ?></p>
										<p class="date">January 2019</p>
									</div>
								</div>
							</div>
							<?php } ?>
						</amp-carousel>
					</div>
				</div>
			</div>
		</section>
		<section class="contact_block">
			<div class="__left"></div>
			<div class="__right">
				<div class="entity">
					<h1 class="header"><?php echo $text_contact1; ?></h1>
					<p class="sub_header"><?php echo $text_contact2; ?></p>
					<form method="post" action-xhr="/index.php?route=common/amp_home/sendmail_amp">
						<div class="line">
							<input placeholder="<?php echo $text_contact3; ?>" name="contact-name">
							<input placeholder="<?php echo $text_contact4; ?>*" name="contact-email">
						</div>
						<textarea placeholder="<?php echo $text_contact5; ?>" name="contact-message"></textarea>
						<button class="btn_simple" type="submit"><?php echo $text_contact6; ?></button>

					</form>
				</div>
			</div>
		</section>
		<section class="insta_block">
			<h3 class="header"><?= $title ?></h3><?php if($account) { ?><a class="sub_header" href="<?= $account['link'] ?>">@<?= $account['username'] ?></a><?php } ?>
			<div class="insta_slider">
				<amp-carousel style="max-width: 425px; width:100%;height: 161px;margin: 0 auto; "  width="507" height="800" autoplay loop
							  delay="4000" layout="responsive" type="slides" >
					<?php foreach ($photos as $photo) { ?>
					<a class="slider__item" href="<?= $photo['link']; ?>" target="_blank">
						<div class="img" style="background-image:url('<?= $photo['image']; ?>')">
							<?php if($photo['type'] == 'video') { ?>
							<span class="video">
                            <i class="fa fa-play"></i>
                        </span>
							<?php } ?>
							<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15">
								<path fill-rule="evenodd" fill="#BBB" d="M10.860,14.999 L4.139,14.999 C1.857,14.999 -0.000,13.142 -0.000,10.860 L-0.000,4.138 C-0.000,1.856 1.857,-0.001 4.139,-0.001 L10.860,-0.001 C13.143,-0.001 15.000,1.856 15.000,4.138 L15.000,10.860 C15.000,13.142 13.143,14.999 10.860,14.999 ZM14.000,3.999 C14.000,2.450 12.549,0.999 11.000,0.999 L4.000,0.999 C2.451,0.999 1.000,2.450 1.000,3.999 L1.000,10.999 C1.000,12.548 2.451,13.999 4.000,13.999 L11.000,13.999 C12.549,13.999 14.000,12.548 14.000,10.999 L14.000,3.999 ZM11.527,4.458 C11.271,4.458 11.019,4.352 10.838,4.171 C10.656,3.990 10.551,3.739 10.551,3.481 C10.551,3.225 10.656,2.972 10.838,2.791 C11.019,2.610 11.271,2.506 11.527,2.506 C11.784,2.506 12.036,2.610 12.217,2.791 C12.399,2.972 12.503,3.225 12.503,3.481 C12.503,3.738 12.399,3.990 12.217,4.171 C12.036,4.352 11.784,4.458 11.527,4.458 ZM7.000,10.999 C4.869,10.999 4.000,9.131 4.000,6.999 C4.000,4.867 5.869,3.999 8.000,3.999 C10.131,3.999 11.000,5.867 11.000,7.999 C11.000,10.130 9.131,10.999 7.000,10.999 ZM7.500,4.965 C6.102,4.965 4.966,6.101 4.966,7.499 C4.966,8.897 6.103,10.034 7.500,10.034 C8.897,10.034 10.034,8.897 10.034,7.499 C10.034,6.101 8.897,4.965 7.500,4.965 Z"
								/>
							</svg>
						</div>
					</a>
					<?php } ?>
				</amp-carousel>
			</div>
		</section>
		<?php echo $amp_footer; ?>