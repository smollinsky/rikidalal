function deviceNav(){
    $('.sendwich').click(function(){
        if($(this).find('.sendwich-btn').is('.active')){
            $(this).find('.sendwich-btn').removeClass('active');
            $(this).find('.sendwich-btn').closest('._top_part').siblings('.menu').removeClass('active');
        }
        else{
            $(this).find('.sendwich-btn').addClass('active');
            $(this).find('.sendwich-btn').closest('._top_part').siblings('.menu').addClass('active');
        }
    });
    $('li.liwithul ul').hide();
};
$(document).ready(function() {
    deviceNav();
    $('input[type=checkbox], select').styler();
    //Menu
    $('header .mobile_header .__right .sendwich.o').click(function() {
        $('header .mobile_header .mobile__menu').addClass('active');
        $('body').css('overflow','hidden');
    })
    $('header .mobile_header .__right .sendwich.c').click(function() {
        $('header .mobile_header .mobile__menu').removeClass('active');
        $('body').css('overflow','');
    })
    // tabs on header line
    $('ul.tabs_caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.container').find('div.tabs_item').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.useful_links_block .all__news').click(function() {
        $('.useful_links_block .services__space').toggleClass('active');
    })
    $('.cams_list_block .tabs .tabs_item .all__news').click(function() {
        $('.cams_list_block .tabs .tabs_item .camera__space').toggleClass('active');
    })


    // tabs on main
    $('ul.tabs_caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.desc_tabs').find('div.tabs_item').removeClass('active').eq($(this).index()).addClass('active');
    });
    $(".top_button").click(function() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    });
    $( window ).scroll(function(){
        if ($(window).scrollTop() < 1000){
            $('.top_button').hide();
        } else {
            $('.top_button').show();
        }
    })

    // Bottom button
    $(document).on('click', '.banner_block .bottom', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 70
        }, 500);
    });

    // Filter mobile
    $('.catalog_block .mobile_filter ._right p,.catalog_block .mobile_filter ._right .op').click(function () {
        $(this).parents('.mobile_filter ').addClass('active');
        $('.catalog_block .fillter').addClass('active');
    })
    $('.catalog_block .mobile_filter ._right .cross').click(function () {
        $(this).parents('.mobile_filter ').removeClass('active');
        $('.catalog_block .fillter').removeClass('active');
        console.log(10);
    })

    // magnific_popap

    $('.search_block .main__list .btn_simple').magnificPopup({
        type: 'inline',
        modal: true,
        alignTop : true,
    });
    $('.search_block .important__list .__item .entity .btn_simple').magnificPopup({
        type: 'inline',
        modal: true,
        alignTop : true,
    });
    $(document).on('click', '.popap_type_one_block .close', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    /**
     * Calculate the masonry
     *
     * Calculate the average of heights of masonry-bricks and then
     * set it as the height of the masonry element.
     *
     * @since 12212018
     * @author Rahul Arora
     * @param grid       Object  The Masonry Element
     * @param gridCell   Object  The Masonry bricks
     * @param gridGutter Integer The Vertical Space between bricks
     * @param dGridCol   Integer Number of columns on big screens
     * @param tGridCol   Integer Number of columns on medium-sized screens
     * @param mGridCol   Integer Number of columns on small screens
     */
    function masonry(grid, gridCell, gridGutter, dGridCol, tGridCol, mGridCol) {
        var g = document.querySelector(grid),
            gc = document.querySelectorAll(gridCell),
            gcLength = gc.length, // Total number of cells in the masonry
            gHeight = 0, // Initial height of our masonry
            i; // Loop counter

        // Calculate the net height of all the cells in the masonry
        for(i=0; i<gcLength; ++i) {
            gHeight+=gc[i].clientHeight+parseInt(gridGutter);
        }

        
         // * Calculate and set the masonry height based on the columns
         // * provided for big, medium, and small screen devices.
         
        if(window.screen.width >= 1024) {
            g.style.height = gHeight/dGridCol + gHeight/(gcLength+1) + "px";
        } else if(window.screen.width < 1024 && window.screen.width >= 768) {
            g.style.height = gHeight/tGridCol + gHeight/(gcLength+1) + "px";
        } else {
            g.style.height = gHeight/mGridCol + gHeight/(gcLength+1) + "px";
        }
    }
    /**
     * Reform the masonry
     *
     * Rebuild the masonry grid on every resize and load event after making sure
     * all the images in the grid are completely loaded.
     */
    ["resize", "load"].forEach(function(event) {
        // Follow below steps every time the window is loaded or resized
        window.addEventListener(event, function () {
            // Check if all the images finished loading
            masonry(".events_block .events_list", ".events_block .events_list .__item,.events_block .events_list .ext", 0, 3, 2, 1);
        });
    });

});
