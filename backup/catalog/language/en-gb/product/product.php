<?php
// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Brand';
$_['text_manufacturer']        = 'Brand:';
$_['text_model']               = 'Style ';
$_['text_reward']              = 'Reward Points:';
$_['text_points']              = 'Price in reward points:';
$_['text_stock']               = 'Availability:';
$_['text_instock']             = 'In Stock';
$_['text_tax']                 = 'Ex Tax:';
$_['text_discount']            = ' or more ';
$_['text_option']              = 'Available Options';
$_['text_minimum']             = 'This product has a minimum quantity of %s';
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews for this product.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_related']             = 'Related Products';
$_['text_tags']                = 'Tags:';
$_['text_error']               = 'Product not found!';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';
$_['text_prev'] = 'Previous';
$_['text_next'] = 'Next style';
$_['text_bride'] = 'brides watched this style in the last';
$_['text_hours'] = 'hours';
$_['text_getprice'] = 'Get price & details';
$_['text_garanted1'] = 'Quality guaranteed';
$_['text_garanted2'] = '100% satisfication when you buy a Riki Dalal dress';
$_['text_garanted3'] = 'Make it personal';
$_['text_garanted4'] = 'We modify the design according to you';
$_['text_specification'] = 'Specification';
$_['text_brides'] = 'Real Brides';
$_['text_brides1'] = 'In this style';
$_['text_discover1'] = 'Discover more';
$_['text_discover2'] = 'You may also love...';

// Entry
$_['entry_qty']                = 'Qty';
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Tabs
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Specification';
$_['tab_review']               = 'Reviews (%s)';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';