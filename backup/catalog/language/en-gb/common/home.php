<?php
// Text
$_['text_search1']          = 'Flagship stores';
$_['text_search2']          = 'We have a lot of Riki Dalal flagship stores';
$_['text_search3']          = 'Enter city/region';
$_['text_search4']          = 'Search';
$_['text_search5']          = 'View all stores';

$_['text_contact1']          = 'Contact Us';
$_['text_contact2']          = 'Contact me today for all of Your fashion needs';
$_['text_contact3']          = 'Name';
$_['text_contact4']          = 'Email';
$_['text_contact5']          = 'Message';
$_['text_contact6']          = 'Send';