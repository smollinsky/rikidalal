<?php
// HTTP
define('HTTP_SERVER', 'http://newlikes.257729-vhost-antosa.gm-host.pp.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://newlikes.257729-vhost-antosa.gm-host.pp.ua/');

// DIR
define('DIR_PATH', '/var/www/257729-antosa/data/www/newlikes.257729-vhost-antosa.gm-host.pp.ua/');
define('DIR_APPLICATION', DIR_PATH . 'catalog/');
define('DIR_SYSTEM', DIR_PATH . 'system/');
define('DIR_IMAGE', DIR_PATH . 'image/');
define('DIR_LANGUAGE', DIR_PATH . 'catalog/language/');
define('DIR_TEMPLATE', DIR_PATH . 'catalog/view/theme/');
define('DIR_TEMPLATE_ELEMENT', DIR_PATH . 'catalog/view/theme/artmar/template/extension/module/element/');
define('DIR_CONTROLLER_ELEMENT', DIR_APPLICATION . 'controller/extension/module/element/');
define('DIR_CONFIG', DIR_PATH . 'system/config/');
define('DIR_CACHE', DIR_PATH . 'system/storage/cache/');
define('DIR_DOWNLOAD', DIR_PATH . 'system/storage/download/');
define('DIR_LOGS', DIR_PATH . 'system/storage/logs/');
define('DIR_MODIFICATION', DIR_PATH . 'system/storage/modification/');
define('DIR_UPLOAD', DIR_PATH . 'system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'testamp');
define('DB_PASSWORD', '8N8r7N5l');
define('DB_DATABASE', 'testamp');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
