<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelEventsEvents extends Model {
    public function addEvent($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "events SET areakm = '" . (int)$data['areakm'] . "', date_from = '" . $data['date_from'] . "', date_to = '" . $data['date_to'] . "', status_event = '" . (int)$data['status_event'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "'");

        $event_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "events SET image = '" . $this->db->escape($data['image']) . "' WHERE event_id = '" . (int)$event_id . "'");
        }

        foreach ($data['event_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "event_description SET event_id = '" . (int)$event_id . "', language_id = '" . (int)$language_id . "', country = '" . $this->db->escape($value['country']) . "', city = '" . $this->db->escape($value['city']) . "', store_name = '" . $this->db->escape($value['store_name']) . "', store_address = '" . $this->db->escape($value['store_address']) . "'");
        }

        return $event_id;
    }

    public function editEvent($event_id, $data) {

        $this->db->query("UPDATE " . DB_PREFIX . "events SET areakm = '" . (int)$data['areakm'] . "', date_from = '" . $data['date_from'] . "', date_to = '" . $data['date_to'] . "', status_event = '" . (int)$data['status_event'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE event_id = '" . (int)$event_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "events SET image = '" . $this->db->escape($data['image']) . "' WHERE event_id = '" . (int)$event_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "event_description WHERE event_id = '" . (int)$event_id . "'");

        foreach ($data['event_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "event_description SET event_id = '" . (int)$event_id . "', language_id = '" . (int)$language_id . "', country = '" . $this->db->escape($value['country']) . "', city = '" . $this->db->escape($value['city']) . "', store_name = '" . $this->db->escape($value['store_name']) . "', store_address = '" . $this->db->escape($value['store_address']) . "'");
        }

    }

    public function copyEvent($event_id){
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "events e LEFT JOIN " . DB_PREFIX . "event_description ed ON (e.event_id = ed.event_id) WHERE e.event_id = '" . (int)$event_id . "' AND ed.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = $query->row;

            $data['status'] = '0';
            $data['areakm'] = '0';
            $data['status_event'] = '0';

            $data['event_description'] = $this->getEventDescriptions($event_id);


            $this->addEvent($data);
        }
    }

    public function deleteEvent($event_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "event_description WHERE event_id = '" . (int)$event_id . "'");

        $this->cache->delete('event');

    }

    public function getEvent($event_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "events e LEFT JOIN " . DB_PREFIX . "event_description ed ON (e.event_id = ed.event_id) WHERE e.event_id = '" . (int)$event_id . "' AND ed.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }


    public function getEvents($data = array()) {
        $sql = "SELECT e.*, ed.* FROM " . DB_PREFIX . "events e LEFT JOIN " . DB_PREFIX . "event_description ed ON (e.event_id = ed.event_id) WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getEventDescriptions($event_id) {
        $event_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "event_description WHERE event_id = '" . (int)$event_id . "'");

        foreach ($query->rows as $result) {
            $event_description_data[$result['language_id']] = array(
                'country'             => $result['country'],
                'city'      => $result['city'],
                'store_name'       => $result['store_name'],
                'store_address'	       => $result['store_address']
            );
        }

        return $event_description_data;
    }


    public function getTotalEvents($data = array()) {
        $sql = "SELECT COUNT(DISTINCT e.event_id) AS total FROM " . DB_PREFIX . "events e LEFT JOIN " . DB_PREFIX . "event_description ed ON (e.event_id = ed.event_id)";

        $sql .= " WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}