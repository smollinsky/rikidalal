<?php
class ModelLocalisationLocation extends Model {
	public function addLocation($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "location SET country = '" . $this->db->escape($data['country']) . "', city = '" . $this->db->escape($data['city']) . "', address = '" . $this->db->escape($data['address']) . "', address1 = '" . $this->db->escape($data['address1']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', email = '" . $this->db->escape($data['email']) . "', whatsapp = '" . $this->db->escape($data['whatsapp']) . "', image = '" . $this->db->escape($data['image']) . "', continent_id = '" . $this->db->escape($data['continent_id']) . "', comment = '" . $this->db->escape($data['comment']) . "', status = '" . $this->db->escape($data['status']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "', has_noya = '" . $this->db->escape($data['has_noya']) . "', has_riki = '" . $this->db->escape($data['has_riki']) . "'");
	
		return $this->db->getLastId();
	}

	public function editLocation($location_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "location SET country = '" . $this->db->escape($data['country']) . "', city = '" . $this->db->escape($data['city']) . "', address = '" . $this->db->escape($data['address']) . "', address1 = '" . $this->db->escape($data['address1']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', email = '" . $this->db->escape($data['email']) . "', whatsapp = '" . $this->db->escape($data['whatsapp']) . "', image = '" . $this->db->escape($data['image']) . "', continent_id = '" . $this->db->escape($data['continent_id']) . "', comment = '" . $this->db->escape($data['comment']) . "', status = '" . $this->db->escape($data['status']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "', has_noya = '" . $this->db->escape($data['has_noya']) . "', has_riki = '" . $this->db->escape($data['has_riki']) . "' WHERE location_id = '" . (int)$location_id . "'");
	}

	public function deleteLocation($location_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = " . (int)$location_id);
	}

	public function getLocation($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

		return $query->row;
	}

        public function getContinent($data = array()) {
            $sql = "SELECT continent_id, continent_name FROM " . DB_PREFIX . "continents";

            $sort_data = array(
                'country',
                'address',
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY continent_NAME";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        }

	public function getLocations($data = array()) {
		$sql = "SELECT location_id, country, address, address1, city, email, telephone FROM " . DB_PREFIX . "location";

		$sort_data = array(
			'country',
			'address',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY country";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalLocations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "location");

		return $query->row['total'];
	}
}
