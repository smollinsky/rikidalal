<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-location" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-location" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country">Country</label>
            <div class="col-sm-10">
              <select name="country" id="input-country" class="form-control">
                <?php foreach ($countries as $countrys) { ?>
                <?php if ($countrys['country_id'] == $country) { ?>
                <option value="<?php echo $countrys['country_id']; ?>" selected="selected"><?php echo $countrys['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $countrys['country_id']; ?>"><?php echo $countrys['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name1">City</label>
            <div class="col-sm-10">
              <input name="city" id="input-name1" value="<?php echo $city; ?>" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name11">Continent</label>
            <div class="col-sm-10">
              <?php foreach ($continents as $continent) { ?>
              <?php if ($continent['continent_id'] == $continent_id) { ?>
              <label><input type="radio" value="<?php echo $continent['continent_id']; ?>" name="continent_id" checked="checked"> <?php echo $continent['continent_name']; ?></label><br>
                <?php } else { ?>
              <label><input type="radio" value="<?php echo $continent['continent_id']; ?>" name="continent_id"> <?php echo $continent['continent_name']; ?></label><br>
                <?php } ?>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address">Collection</label>
            <div class="col-sm-10">
              <textarea type="text" name="address" placeholder="<?php echo $entry_address; ?>" rows="5" id="input-address" class="form-control"><?php echo $address; ?></textarea>
              <?php if ($error_address) { ?>
              <div class="text-danger"><?php echo $error_address; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-address1">Address</label>
            <div class="col-sm-10">
              <textarea type="text" name="address1" placeholder="<?php echo $entry_address; ?>" rows="5" id="input-address1" class="form-control"><?php echo $address1; ?></textarea>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
            <div class="col-sm-10">
              <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
              <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php echo $error_telephone; ?></div>
              <?php  } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-fax">Email</label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="" id="input-fax" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-fax3">Whatsapp</label>
            <div class="col-sm-10">
              <input type="text" name="whatsapp" value="<?php echo $whatsapp; ?>" placeholder="" id="input-fax3" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
            <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-comment"><span data-toggle="tooltip" data-container="#content" title="<?php echo $help_comment; ?>"><?php echo $entry_comment; ?></span></label>
            <div class="col-sm-10">
              <textarea name="comment" rows="5" placeholder="<?php echo $entry_comment; ?>" id="input-comment" class="form-control"><?php echo $comment; ?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name11">Flagship Boutiques</label>
            <div class="col-sm-10">
              <?php if ($status == 1) { ?>
              <label><input type="radio" value="1" name="status" checked>Yes  </label>
              <label><input type="radio" value="0" name="status" >No</label>
            <?php } else { ?>
              <label><input type="radio" value="1" name="status" >Yes</label>
            <label><input type="radio" value="0" name="status" checked>No</label>
            <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name11">Has Noya</label>
            <div class="col-sm-10">
              <?php if ($has_noya == 0) { ?>
              <label><input type="radio" value="1" name="has_noya" >Yes  </label>
              <label><input type="radio" value="0" name="has_noya" checked>No</label>
            <?php } else { ?>
              <label><input type="radio" value="1" name="has_noya" checked>Yes</label>
            <label><input type="radio" value="0" name="has_noya" >No</label>
            <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name11">Has Riki</label>
            <div class="col-sm-10">
              <?php if ($has_riki == 0) { ?>
              <label><input type="radio" value="1" name="has_riki" >Yes  </label>
              <label><input type="radio" value="0" name="has_riki" checked>No</label>
            <?php } else { ?>
              <label><input type="radio" value="1" name="has_riki" checked>Yes</label>
            <label><input type="radio" value="0" name="has_riki" >No</label>
            <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-fax5">Sort order</label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="" id="input-fax5" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>

