<?php

class ControllerEventsEvents extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('events/events');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('events/events');

        $this->getList();
    }

    public function add() {
        $this->load->language('events/events');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('events/events');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_events_events->addEvent($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('events/events');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('events/events');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_events_events->editEvent($this->request->get['event_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('events/events');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('events/events');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $event_id) {
                $this->model_events_events->deleteEvent($event_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    public function copy() {
        $this->load->language('events/events');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('events/events');

        if (isset($this->request->post['selected']) && $this->validateCopy()) {
            foreach ($this->request->post['selected'] as $event_id) {
                $this->model_events_events->copyEvent($event_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('events/events/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['copy'] = $this->url->link('events/events/copy', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('events/events/delete', 'token=' . $this->session->data['token'] . $url, true);

        $data['enabled'] = $this->url->link('events/events/enable', 'token=' . $this->session->data['token'] . $url, true);
        $data['disabled'] = $this->url->link('events/events/disable', 'token=' . $this->session->data['token'] . $url, true);

        $data['events'] = array();

        $filter_data = array(
            'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'           => $this->config->get('config_limit_admin')
        );

        $this->load->model('tool/image');

        $event_total = $this->model_events_events->getTotalEvents($filter_data);

        $results = $this->model_events_events->getEvents($filter_data);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 40, 40);
            }

            $data['events'][] = array(
                'event_id' => $result['event_id'],
                'image'      => $image,
                'country' => $result['country'],
                'city' => $result['city'],
                'store_name' => $result['store_name'],
                'store_address' => $result['store_address'],
                'date_from' => $result['date_from'],
                'date_to' => $result['date_to'],
                'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
                'href_shop'  => HTTP_CATALOG . 'index.php?route=events/events&event_id=' . ($result['event_id']),
                'edit'       => $this->url->link('events/events/edit', 'token=' . $this->session->data['token'] . '&event_id=' . $result['event_id'] . $url, true)
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_noindex'] = $this->language->get('column_noindex');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_noindex'] = $this->language->get('entry_noindex');

        $data['button_copy'] = $this->language->get('button_copy');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_shop'] = $this->language->get('button_shop');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['button_enable'] = $this->language->get('button_enable');
        $data['button_disable'] = $this->language->get('button_disable');

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_order'] = $this->url->link('events/events', 'token=' . $this->session->data['token'] . '&sort=e.sort_order' . $url, true);

        $url = '';

        $pagination = new Pagination();
        $pagination->total = $event_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('events/events', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('events/event_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['article_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_select'] = $this->language->get('text_select');
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_image_add'] = $this->language->get('button_image_add');
        $data['button_remove'] = $this->language->get('button_remove');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_design'] = $this->language->get('tab_design');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['meta_h1'])) {
            $data['error_meta_h1'] = $this->error['meta_h1'];
        } else {
            $data['error_meta_h1'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['event_id'])) {
            $data['action'] = $this->url->link('events/events/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('events/events/edit', 'token=' . $this->session->data['token'] . '&event_id=' . $this->request->get['event_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('events/events', 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['event_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $event_info = $this->model_events_events->getEvent($this->request->get['event_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['event_description'])) {
            $data['event_description'] = $this->request->post['event_description'];
        } elseif (isset($this->request->get['event_id'])) {
            $data['event_description'] = $this->model_events_events->getEventDescriptions($this->request->get['event_id']);
        } else {
            $data['event_description'] = array();
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($event_info)) {
            $data['image'] = $event_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($article_info) && is_file(DIR_IMAGE . $event_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($event_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


        if (isset($this->request->post['date_from'])) {
            $data['date_from'] = $this->request->post['date_from'];
        } elseif (!empty($event_info)) {
            $data['date_from'] = ($event_info['date_from'] != '0000-00-00') ? $event_info['date_from'] : '';
        } else {
            $data['date_from'] = date('Y-m-d');
        }
        if (isset($this->request->post['date_to'])) {
            $data['date_to'] = $this->request->post['date_to'];
        } elseif (!empty($event_info)) {
            $data['date_to'] = ($event_info['date_to'] != '0000-00-00') ? $event_info['date_to'] : '';
        } else {
            $data['date_to'] = date('Y-m-d');
        }
        

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($event_info)) {
            $data['sort_order'] = $event_info['sort_order'];
        } else {
            $data['sort_order'] = 1;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($event_info)) {
            $data['status'] = $event_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['status_event'])) {
            $data['status_event'] = $this->request->post['status_event'];
        } elseif (!empty($event_info)) {
            $data['status_event'] = $event_info['status_event'];
        } else {
            $data['status_event'] = 1;
        }

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('events/event_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'events/events')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['event_description'] as $language_id => $value) {
            if ((utf8_strlen($value['country']) < 3) || (utf8_strlen($value['country']) > 255)) {
                $this->error['country'][$language_id] = $this->language->get('error_name');
            }

            if ((utf8_strlen($value['city']) < 2) || (utf8_strlen($value['city']) > 255)) {
                $this->error['city'][$language_id] = $this->language->get('error_meta_title');
            }

            if ((utf8_strlen($value['store_name']) < 3) || (utf8_strlen($value['store_name']) > 255)) {
                $this->error['store_name'][$language_id] = $this->language->get('error_meta_h1');
            }
            if ((utf8_strlen($value['store_address']) < 3) || (utf8_strlen($value['store_address']) > 255)) {
                $this->error['store_address'][$language_id] = $this->language->get('error_meta_h1');
            }

        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }


    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'events/events')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateCopy() {
        if (!$this->user->hasPermission('modify', 'events/events')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('events/events');

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name'  => $filter_name,
                'start'        => 0,
                'limit'        => $limit
            );

            $results = $this->model_events_events->getArticles($filter_data);

            foreach ($results as $result) {

                $json[] = array(
                    'article_id' => $result['article_id'],
                    'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}